﻿using Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayThisMusic : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SoundSystem.PlayMusic(Sounds.instance.music_gameplay_theme1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
