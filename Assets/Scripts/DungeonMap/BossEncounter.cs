﻿using UnityEngine;

namespace Game.Dungeon
{
	[CreateAssetMenu(menuName = "Game/Map/BossEncounter", fileName = "BossEncounter")]
	public class BossEncounter : BattleEncounter
	{

	}
}