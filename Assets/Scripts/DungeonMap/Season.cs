﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Dungeon
{
	[CreateAssetMenu(menuName = "Game/Map/Season", fileName = "Season")]
	public class Season : ScriptableObject
	{
		public string Key;
		public float Progress;
		public List<Encounter> Encounters;
	}
}