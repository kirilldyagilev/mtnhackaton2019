﻿using UnityEngine;

namespace Game.Dungeon
{
	public abstract class Encounter : ScriptableObject
	{
		public string EncounterName = "Encounter1";
		public int Id;
		public EndEncounterMessage.Type EndType = EndEncounterMessage.Type.Next;
		public int HP, ATK, STM;
		public virtual void Start()
		{
			SceneLoader.Instance.LoadScene(EncounterName);
			EventBus<PlayerDeathMessage>.Sub(HandleDeath);
			EventBus<WinMessage>.Sub(HandleWin);
		}

		public virtual void End(bool dead, string perk)
		{
			EventBus<EndEncounterMessage>.Pub(new EndEncounterMessage() { 
				EndType = dead? EndEncounterMessage.Type.Death : EndType, EncounterId = Id , Perk = perk,
				Health = HP, Attack = ATK, Stamina = STM
			});
			EventBus<PlayerDeathMessage>.Unsub(HandleDeath);
			EventBus<WinMessage>.Unsub(HandleWin);
		}

		private void HandleDeath(PlayerDeathMessage message)
		{
			End(true, null);
		}
		private void HandleWin(WinMessage message)
		{
			End(false, message.Perk);
		}
	}
}