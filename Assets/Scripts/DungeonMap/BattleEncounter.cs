﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Game.Dungeon
{
	[CreateAssetMenu(menuName = "Game/Map/BattleEncounter", fileName = "BattleEncounter")]
	public class BattleEncounter : Encounter
	{
		[System.Serializable]
		public class Zone
		{
			public Vector2Int anchor;
			public Vector2Int size;
			public Vector2Int prefabSize = Vector2Int.one;
			public List<GameObject> Prefabs;
		}
  
		public List<Zone> Zones;
		public List<GameObject> SmallPrefabs;
		public int SmallPrefabsCount = 28;

		public List<BaseMobController> Mobs;
		public Vector2Int MobsZoneAnchor;
		public Vector2Int MobsZoneSize;

		public override void Start()
		{
			SceneLoader.Instance.OnEndLoading += OnSceneLoad;
			base.Start();
		}

		private void OnSceneLoad()
		{
			SceneLoader.Instance.OnEndLoading -= OnSceneLoad;
			var sqrtoftwo = Mathf.Sqrt(2) *.5f;
			foreach (var zone in Zones)
			{
				var position = zone.anchor + (Vector2)zone.prefabSize * .5f + new Vector2(Random.value * zone.size.x, Random.value * zone.size.y);
				var buffer = new Vector2(position.x * sqrtoftwo - position.y * sqrtoftwo, sqrtoftwo * (position.x + position.y));
				var prefab = zone.Prefabs[Random.Range(0, zone.Prefabs.Count)];
				Instantiate(prefab, new Vector3(buffer.x, .5f, buffer.y), Quaternion.Euler(0, Random.value * 360, 0));
			}
			if (SmallPrefabs.Count > 0)
			{
				for (int i = 0; i < SmallPrefabsCount; i++)
				{
					var position = new Vector2(Random.value * 30 , Random.value * 30);
					var buffer = new Vector2(position.x * sqrtoftwo - position.y * sqrtoftwo, sqrtoftwo * (position.x + position.y));
					var prefab = SmallPrefabs[Random.Range(0, SmallPrefabs.Count)];
					Instantiate(prefab, new Vector3(buffer.x, 1.5f, buffer.y), Quaternion.Euler(0,Random.value * 360,0));
				}
			}
			//foreach (var mob in Mobs)
			//{
			//	var position = MobsZoneAnchor + new Vector2(Random.value * MobsZoneSize.x, Random.value * MobsZoneSize.y);

			//	Instantiate(mob.gameObject, new Vector3(position.x, 2, position.y), Quaternion.Euler(0, Random.value * 360, 0));
			//}
		}
	}
}