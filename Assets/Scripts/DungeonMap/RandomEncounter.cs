﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Dungeon
{
	[CreateAssetMenu(menuName = "Game/Map/RandomEncounter", fileName = "RandomEncounter")]
	public class RandomEncounter : Encounter
	{
		public List<Encounter> Encounters;
		public override void Start()
		{
			var index = Random.Range(0, Encounters.Count);
			Encounters[index].Start();
		}
	}
}