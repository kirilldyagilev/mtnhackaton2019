﻿using System.Collections.Generic;

namespace Game.Dungeon
{
	public class EndEncounterMessage : Message
	{
		public enum Type
		{
			Death,
			Next,
			Complete,
			SecondComics,
			FinalComics
		}
		public Type EndType;
		public int EncounterId;
		public string Perk;

		public int Health;
		public int Attack;
		public int Stamina;
	}
}