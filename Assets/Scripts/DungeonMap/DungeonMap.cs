﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Dungeon
{
	[CreateAssetMenu(menuName = "Game/Map/DungeonMap", fileName = "DungeonMap")]
	public class DungeonMap : ScriptableObject
	{
		public List<Season> Seasons;
	}
}