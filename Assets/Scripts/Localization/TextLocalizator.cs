﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

namespace Game
{
	[RequireComponent(typeof(TMP_Text))]
	public class TextLocalizator : Handler<ChangedLanguageMessage>
	{
		[SerializeField] private string key = string.Empty;
		private TMPro.TMP_Text text;
		protected override void Awake()
		{
			text = GetComponent<TMP_Text>();
			base.Awake();
			HandleMessage(new ChangedLanguageMessage());
		}

		public override void HandleMessage (ChangedLanguageMessage message)
		{
			text.text = LocalizationSystem.Text[key].Replace("\\\"","\"");
		}
	}
}