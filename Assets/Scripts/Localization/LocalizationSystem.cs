﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public static class LocalizationSystem
	{
		public const string DEFAULT_LANGUAGE = "English";
		public class DataHolder
		{
			public Dictionary<string, string> _database = new Dictionary<string, string>();
			public List<string> AvaibleLanguages = new List<string>();
			public string SelectedLanguage = DEFAULT_LANGUAGE;
		}
		public class Indexer
		{
			public string this[string key] => GetText(key);
		}

		public static Indexer Text
		{
			get;
			private set;
		} = new Indexer();
		private static DataHolder _dataHolder = null;
		public static List<string> AvaibleLanguages;
		public static string SelectedLanguage => _dataHolder.SelectedLanguage;

		static LocalizationSystem()
		{
			_dataHolder = new DataHolder();
			AvaibleLanguages = new List<string>() {"Russian", "English" };
			Initialize();
		}

		private static void Initialize()
		{
			var languageSelected = PlayerPrefs.GetInt("DefaultLanguageSet", 0) == 1;
			if (!languageSelected)
			{
				PlayerPrefs.SetInt("DefaultLanguageSet", 1);
				var systemLanguage = GetSystemLanguage();
				PlayerPrefs.SetInt("language", LanguageToIndex(systemLanguage));
				PlayerPrefs.Save();
			}
			_dataHolder.SelectedLanguage = IndexToLanguage(PlayerPrefs.GetInt("language", 0));
			LoadLanguage(SelectedLanguage);
		}

		private static int LanguageToIndex(string language)
		{
			switch (language)
			{
				case "Russian":
					return 1;
				case "English":
				default:
					return 0;
			}
		}

		private static string IndexToLanguage(int index)
		{
			switch (index)
			{
				case 1:
					return "Russian";
				case 0:
				default:
					return "English";
			}
		}

		public static string ShortLanguageToLanguage(string shortLanguage)
		{
			switch (shortLanguage)
			{
				case "ru":
					return "Russian";
				case "en":
				default:
					return "English";
			}
		}

		public static string LanguageToShortLanguage(string shortLanguage)
		{
			switch (shortLanguage)
			{
				case "Russian":
					return "ru";
				case "English":
				default:
					return "en";
			}
		}

		public static string GetLanguage()
		{
			return SelectedLanguage;
		}

		public static void SetLanguage(string language)
		{
			if (!AvaibleLanguages.Contains(language))
			{
				Debug.LogError($"[Localization] - Language \"{language}\" not found");
				return;
			}

			if (SelectedLanguage == language)
			{
				Debug.LogWarning($"[Localization] - Language \"{language}\" already in use");
				return;
			}

			_dataHolder.SelectedLanguage = language;

			PlayerPrefs.SetInt("language", LanguageToIndex(SelectedLanguage));
			PlayerPrefs.Save();

			LoadLanguage(language);
			EventBus<ChangedLanguageMessage>.Pub(new ChangedLanguageMessage());
		}

		private static void LoadLanguage(string language)
		{
			_dataHolder._database.Clear();
			TextAsset text = Resources.Load<TextAsset>(SelectedLanguage);
			var json = JSONObject.Create(text.text);
			foreach (var key in json.keys)
			{
				_dataHolder._database.Add(key, json[key].str);
			}
		}

		public static string GetText(string key)
		{
			if (string.IsNullOrEmpty(key))
			{
				return key;
			}
			return _dataHolder._database.TryGetValue(key, out string text) ? text : key;
		}

		private static string GetSystemLanguage()
		{
			var culture = Application.systemLanguage;
			switch (culture)
			{
				case SystemLanguage.Russian:
					return "Russian";
				default:
					return "English";
			}
		}
	}
}