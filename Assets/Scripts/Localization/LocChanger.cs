﻿using UnityEngine.Events;

namespace Game
{
	public class LocChanger : Handler<ChangedLanguageMessage>
	{
		public UnityEvent OnRu;
		public UnityEvent OnEn;


		protected override void Awake()
		{
			base.Awake();
			HandleMessage(new ChangedLanguageMessage());
		}

		public void ChangeLanguage()
		{
			if (LocalizationSystem.GetLanguage() == "English")
			{
				LocalizationSystem.SetLanguage("Russian");
			}
			else
			{
				LocalizationSystem.SetLanguage("English");
			}
		}

		public override void HandleMessage(ChangedLanguageMessage message)
		{
			if(LocalizationSystem.GetLanguage() == "English")
			{
				OnEn?.Invoke();
			}
			else
			{
				OnRu?.Invoke();
			}
		}
	}
}