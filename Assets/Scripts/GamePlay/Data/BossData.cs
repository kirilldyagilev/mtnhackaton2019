﻿using UnityEngine;

namespace Game
{
	[CreateAssetMenu(menuName = "Data/BossData")]
	public class BossData : EnemyData
	{
		public int[] StateHealth;
		public float[] StateAttackDelay;
		public int[] StateSpeed;
	}
}