﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    [Serializable]
    public class PerkData
    {
        public string key;
        public int weight;
        public Image icon;
    }

    [CreateAssetMenu(menuName = "Data/PerksTableData")]
    public class PerksTableData : ScriptableObject
    {
        public List<PerkData> perks = new List<PerkData>();
    }
}