﻿using UnityEngine;

namespace Game
{
	public class MobData : ScriptableObject
	{
		public int Health;
		public int Speed;
		public float AttackDelay;
		public int Strength;
	}
}