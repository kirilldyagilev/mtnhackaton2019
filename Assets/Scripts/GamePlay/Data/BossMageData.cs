﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(menuName = "Data/BossMageData")]
    public class BossMageData : BossData
    {
        public int[] ProjectileNum;
        public int[] ProjectileSpread;
    }
}
