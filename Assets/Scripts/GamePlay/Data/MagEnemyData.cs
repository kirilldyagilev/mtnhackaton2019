﻿using UnityEngine;

namespace Game
{
    [CreateAssetMenu(menuName = "Data/MagEnemyData")]
    public class MagEnemyData : EnemyData
    {
        public int projectiles;
        public int spreadAngle;
    }
}
