﻿using System.Collections.Generic;

using UnityEngine;

namespace Game
{
	[CreateAssetMenu(menuName = "Data/EnemyData")]
	public class EnemyData : MobData
	{
		public float Distance;
		public List<string> AttackList;
		public float AttackSpeed;
        public float VisionDistance;
	}
}