﻿using UnityEngine;

namespace Game
{
	[CreateAssetMenu(menuName = "Data/PlayerData")]
	public class PlayerData : MobData
	{
		public int Stamina;
		public int StaminaRegeneration;
		public float StaminaMoveRegenerationCoeff;
		public float RollSpeed;
		public float RollDuration;
		public float RollCost;
        public float AttackCost;
		public float SpeedWhenReloadCoeff;
	}
}