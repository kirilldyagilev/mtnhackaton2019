﻿using System;

using UnityEngine;

namespace Game.Archer
{
	public class ArcherController : BaseMobController
	{

        public override void SetDamage(int damage)
        {
            base.SetDamage(damage);

            SoundSystem.PlaySound(
                new AudioClip[] { Sounds.instance.snd_enemy_shooter_hit1, Sounds.instance.snd_enemy_shooter_hit2, Sounds.instance.snd_enemy_shooter_hit3 },
                transform.position
            );
        }
    }
}