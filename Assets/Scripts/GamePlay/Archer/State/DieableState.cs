﻿namespace Game.Archer
{
	public class DieableState : State
	{
		public override void Damage(int damage)
		{
			GetComponent<IController>().SetDamage(damage);
		}
	}
}