﻿using System;

using UnityEngine;
using UnityEngine.AI;

namespace Game.Archer
{
	public class AttackState : DieableState
	{
		private NavMeshAgent _navMeshAgent;
		public Animator Animator;
		
		private void Awake()
		{
			_navMeshAgent = GetComponent<NavMeshAgent>();
		}

		public override void StateEnter()
		{
			_navMeshAgent.SetDestination(transform.position);
			Animator.SetBool("Run", false);
		}

		public override void MoveToTarget(Vector3 target)
		{
			_navMeshAgent.SetDestination(transform.position);
			Animator.SetBool("Run", false);
		}
	}
}