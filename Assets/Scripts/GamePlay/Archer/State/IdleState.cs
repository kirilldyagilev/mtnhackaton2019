﻿using UnityEngine;
using UnityEngine.AI;

namespace Game.Archer
{
	[RequireComponent(typeof(NavMeshAgent))]
	public class IdleState : DieableState
	{
		private IAttacker _attacker = null;
		private ArcherController _archerController = null;
		private Rigidbody _rigidBody = null;
		private NavMeshAgent _navMeshAgent;
		private Vector3 previousTargetPosition = Vector3.zero;

		public Animator Animator = null;
		public Notifier Notifier = null;

		[SerializeField] private AttackState _attackState = null;

		private void Awake()
		{
			_attacker = GetComponent<IAttacker>();
			_rigidBody = GetComponent<Rigidbody>();
			_archerController = GetComponent<ArcherController>();
			_navMeshAgent = GetComponent<NavMeshAgent>();
		}

		public override void Attack(Vector3 attackDirection)
		{
			var dir = (PlayerController.Instance.transform.position - transform.position);
			dir.y = 0;
			_attacker.StartAttack(dir.normalized, LayerMask.NameToLayer("Player"));
			_attacker.SetAttackSpeed(_archerController.Data.AttackSpeed);
			_archerController.ChangeState(_attackState);
			Invoke(nameof(AfterAttackDelay), _archerController.Data.AttackDelay);
			Animator.SetTrigger("Shoot");

			if (Notifier != null)
			{
				Notifier.Notify(0);
			}
		}

		private void AfterAttackDelay()
		{
			_archerController.CheckState();
		}

		public override void Move(Vector3 direction, float speed)
		{
			if (Mathf.Approximately(direction.sqrMagnitude, 0))
			{
				return;
			}
			var newPosition = transform.position + speed * Time.deltaTime * direction;
			_rigidBody.MovePosition(newPosition);

			var newRotation = Quaternion.LookRotation(direction, Vector3.up);
			_rigidBody.MoveRotation(newRotation);
		}

		public override void MoveToTarget(Vector3 target)
		{
			if (Vector3.SqrMagnitude(previousTargetPosition - target) > 0.1f)
			{
				_navMeshAgent.stoppingDistance = 1f;
				_navMeshAgent.speed = _archerController.Data.Speed;
				_navMeshAgent.SetDestination(target);
				previousTargetPosition = target;
				Animator.SetBool("Run", true);
				return;
			}
		}
	}
}