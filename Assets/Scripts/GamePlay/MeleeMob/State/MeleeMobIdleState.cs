﻿using UnityEngine;
using Game.Archer;
using UnityEngine.AI;

namespace Game
{
    [RequireComponent(typeof(NavMeshAgent))]
    class MeleeMobIdleState : DieableState
    {
        private IAttacker _attacker = null;
        private MeleeMobController _controller;
        private NavMeshAgent _navMeshAgent;
        private Vector3 previousTargetPosition = Vector3.zero;
        [SerializeField] private State _attackState = null;

        private void Awake()
        {
            _attacker = GetComponent<IAttacker>();
            _controller = GetComponent<MeleeMobController>();
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        public override void Attack(Vector3 attackDirection)
        {
            _controller.ChangeState(_attackState);
            Invoke(nameof(AfterAttackDelay), _controller.Data.AttackDelay);
            _attacker.StartAttack((transform.position - PlayerController.Instance.transform.position).normalized, LayerMask.NameToLayer("Player"));
        }

        public override void Move(Vector3 directon, float speed)
        {
            transform.position += directon * speed * Time.deltaTime;
            Quaternion targetRotation = Quaternion.LookRotation(directon);
            targetRotation.z = 0;
            targetRotation.x = 0;
            transform.rotation = targetRotation;
        }
        
        public override void MoveToTarget(Vector3 target)
        {

            if (Vector3.SqrMagnitude(previousTargetPosition - target) > 0.1f)
            {
                _navMeshAgent.stoppingDistance = 1f;
                _navMeshAgent.speed = _controller.Data.Speed;
                _navMeshAgent.SetDestination(target);
                previousTargetPosition = target;
            }
        }

        private void AfterAttackDelay()
        {
            if (_controller.GetState() == _attackState)
                _controller.ChangeState(this);
        }

    }
}
