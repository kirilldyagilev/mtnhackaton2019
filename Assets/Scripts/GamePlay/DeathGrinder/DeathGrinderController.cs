﻿using System;

using Game;

using UnityEngine;

namespace GamePlay.DeathGrinder
{
	public class DeathGrinderController : MonoBehaviour, IDamageController
	{
		public int AttackValue = 0;
		public int Health = 3;

		private void OnTriggerEnter(Collider other)
		{
			EventBus<DamagePlayer>.Pub(new DamagePlayer() { Damage = AttackValue});
		}

		public void TryDamage(int damageValue)
		{
			Health -= damageValue;
		}
	}
}