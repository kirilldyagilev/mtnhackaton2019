﻿using System;

using UnityEngine;

namespace Game
{
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] private PlayerController _playerController = null;

        private void FixedUpdate()
        {
            var horizontal = Input.GetAxis("Horizontal");
            var vertical = Input.GetAxis("Vertical");

            var direction = new Vector3(horizontal, 0, vertical);
            
            //var attackDirection = new Vector3();

            //Inputs
            direction.Normalize();
            _playerController.TryMove(direction);
		}

		private void Update()
		{
			float hitDist = 0.0f;
			var horizontal = Input.GetAxis("Horizontal");
			var vertical = Input.GetAxis("Vertical");
			var rollDirection = new Vector3(horizontal, 0, vertical);
			
			if (Input.GetKeyDown(KeyCode.Space))
			{
				_playerController.TryRoll(rollDirection.normalized);
			}
			if (Input.GetKey(KeyCode.F))
			{
				_playerController.TryAttack(transform.forward);
			}

			if (Input.GetKey(KeyCode.Mouse0))
			{
				Plane playerPlane = new Plane(Vector3.up, Vector3.up*2);
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                
				if(playerPlane.Raycast(ray, out hitDist))
				{
					Vector3 targetPoint = ray.GetPoint(hitDist);
					Debug.DrawLine(targetPoint, targetPoint + Vector3.up * 5);
					targetPoint -= _playerController.transform.position;
					Vector3 attackDir = new Vector3(targetPoint.x, 0, targetPoint.z);
					attackDir.Normalize();
					_playerController.TryAttack(attackDir);
				}
			}
		}
	}
}