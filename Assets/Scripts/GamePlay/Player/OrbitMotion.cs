﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitMotion : MonoBehaviour
{
    float Timer = 0;
    public float Radius = 5;
    public float Speed = 50;

    private Vector3 _originPosition;

    // Update is called once per frame
    private void Awake()
    {
        _originPosition = transform.position;
    }

    void Update()
    {
        Timer += Time.deltaTime * Speed;
        transform.position = _originPosition + Quaternion.AngleAxis(Timer, Vector3.up) * new Vector3(Radius, 2f);
        transform.rotation = Quaternion.AngleAxis(Timer, Vector3.up);
    }
}
