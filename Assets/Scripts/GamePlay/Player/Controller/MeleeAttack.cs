﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game
{
    class MeleeAttack: MonoBehaviour
    {
        private Animator animator;
        public int _damage = 0;
        public LayerMask _targetLayer;

        public void SetDirection(Vector3 direction)
        {
            Quaternion targetRotation = Quaternion.LookRotation(direction - transform.position.normalized);
            targetRotation.z = 0;
            targetRotation.x = 0;
            transform.rotation = targetRotation;
        }

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        public void Update()
        {
            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
            {
                animator.StopPlayback();
                Destroy(gameObject);
            }
            
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer != _targetLayer)
            {
                return;
            }

            var controller = other.GetComponent<IController>();
            if (controller == null)
            {
                //Destroy(gameObject);
                return;
            }

            if (controller.GetState() is RollState)
            {
                return;
            }

            var damageController = GetComponent<IDamageController>();
            if (damageController != null)
            {
                //Destroy(gameObject);
                return;
            }

            EventBus<DamageMessage>.Pub(new DamageMessage
            {
                DamageController = other.GetComponent<IDamageController>(),
                DamageValue = _damage,
                target = other.transform,
                attacker = transform
            });
        }
    }
}
