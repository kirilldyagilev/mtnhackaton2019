﻿using UnityEngine;

namespace Game
{
	public interface IDamageController
	{
		void TryDamage(int damageValue);
    }
}