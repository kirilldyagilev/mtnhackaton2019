﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GamePlay.Weapon;

using UnityEngine;

namespace Game
{
	public interface IAttacker
	{
		void StartAttack(Vector3 direction, LayerMask layerMask);
		void WeaponAttack();
		void WeaponEndAttack();
		void SetAttackSpeed(float multiply);
		BaseWeapon GetWeapon();
	}
}
