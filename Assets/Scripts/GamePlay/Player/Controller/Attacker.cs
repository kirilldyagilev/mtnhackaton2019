﻿using System.Collections.Generic;

using GamePlay.Weapon;

using UnityEngine;

namespace Game
{
	public class Attacker : MonoBehaviour, IAttacker
	{
		[SerializeField] private BaseWeapon _currentWeapon = null;
		public BaseWeapon Weapon => _currentWeapon;

		public Animator _weaponAnimator = null;
		public Animator _characterAnimator = null;
		
		public void StartAttack(Vector3 direction, LayerMask layerMask)
		{
			_currentWeapon.StartFire(direction, layerMask);
			_currentWeapon.SetDamage(GetComponent<IController>().GetStrength());
		}

		public void WeaponAttack()
		{
			_currentWeapon.Fire();
		}

		public void WeaponEndAttack()
		{
			_currentWeapon.StopFire();
		}

		public void SetAttackSpeed(float multiply)
		{
			if (_weaponAnimator != null)
			{
				_weaponAnimator.SetFloat("SpeedMultiply", multiply);
			}

			if (_characterAnimator != null)
			{
				_characterAnimator.SetFloat("SpeedMultiply", multiply);
			}
		}

		public BaseWeapon GetWeapon()
		{
			return _currentWeapon;
		}
	}
}