﻿using System;
using System.Collections;

using UnityEngine;

namespace Game
{
	public class PlayerController : MonoBehaviour, IDamageController, IController
	{
		[SerializeField] private PlayerClientData _playerClientData = null;
		[SerializeField] private State _currentState = null;
		[SerializeField] private State _deathState = null;
		[SerializeField] private State _afterAttackState = null;
		[SerializeField] private State _liveState = null;

		public static PlayerController Instance;
		public PlayerClientData Data => _playerClientData;
        private PerkController perkController = null;
		public int Health;
		public int Attack;
		public int Stamina;
		public Action OnStaminaNeeded = null;

        private Rigidbody _rigidBody;
		public bool Godlike = false;

		private void Awake()
		{
			Instance = this;
			_rigidBody = GetComponent<Rigidbody>();
			Godlike = false;
			EventBus<MobDeathMessage>.Sub(HandleMessage);
		}

		private void OnDestroy()
		{
			Instance = null;
			EventBus<MobDeathMessage>.Unsub(HandleMessage);
		}

		private void HandleMessage(MobDeathMessage message)
		{
			var rand = UnityEngine.Random.value;
			if (rand < 1f / 3)
			{
				Health++;
			}
			else if(rand<2f/3)
			{
				Attack++;
			}
			else
			{
				Stamina++;
			}
		}
		public void ChangeState(State newState)
		{
			_currentState = newState;
			_currentState.StateEnter();
		}

		public void TryAttack(Vector3 direction)
		{
			_currentState.Attack(direction);
		}

		public void TryMove(Vector3 direction)
		{
			_currentState.Move(direction, _playerClientData.Speed);
		}

		public void TryDamage(int damage)
		{
			_currentState.Damage(damage);
		}

		public void TryRoll(Vector3 rollDirection)
		{
			_currentState.Roll(rollDirection);
		}

		public void Roll(Vector3 rollDirection)
		{
			StartCoroutine(RollCoroutine(rollDirection));
		}

		public IEnumerator RollCoroutine(Vector3 rollDirection)
		{
            SoundSystem.PlaySound(
                    new AudioClip[] {
                        Sounds.instance.snd_player_move_roll1,
                        Sounds.instance.snd_player_move_roll2,
                        Sounds.instance.snd_player_move_roll3
                    },
                    transform.position
                );

            var duration = _playerClientData.RollDuration;
			var rollSpeed = _playerClientData.RollSpeed;
			var currentDuration = 0f;
			while(currentDuration < duration)
			{
				currentDuration += Time.fixedDeltaTime;
                if (Mathf.Approximately(rollDirection.sqrMagnitude, 0))
                {
                    _rigidBody.MovePosition(transform.position + transform.forward * rollSpeed * Time.fixedDeltaTime);
                }
                else
                {
                    _rigidBody.MovePosition(transform.position + rollDirection * rollSpeed * Time.fixedDeltaTime);
                }				
				yield return new WaitForFixedUpdate();
			}

			CheckState();
		}
		
		public void CheckState()
		{
			ChangeState(_playerClientData.Health > 0 ? _liveState : _deathState);
		}

		public State GetState()
		{
			return _currentState;
		}

		public void SetDamage(int damage)
		{
            SoundSystem.PlaySound(
                new AudioClip[] {
                    Sounds.instance.snd_player_damage1,
                    Sounds.instance.snd_player_damage2,
                    Sounds.instance.snd_player_damage3,
                    Sounds.instance.snd_player_damage4,
                    Sounds.instance.snd_player_damage5,

                    Sounds.instance.snd_player_hit1,
                    Sounds.instance.snd_player_hit2,
                    Sounds.instance.snd_player_hit3,
                    Sounds.instance.snd_player_hit4
                },
                transform.position
            );
			if (Godlike)
			{
				return;
			}
            _playerClientData.Health.Value -= damage;
			ProfileOchestrator.CurrentHP = _playerClientData.Health.Value;
			if (_playerClientData.Health.Value <= 0)
			{
				ChangeState(_deathState);

                SoundSystem.PlaySound(
                    new AudioClip[] {
                        Sounds.instance.snd_player_death1,
                        Sounds.instance.snd_player_death2,
                        Sounds.instance.snd_player_death3,
                        Sounds.instance.snd_player_death4,
                        Sounds.instance.snd_player_death5
                    },
                    transform.position
                );

            } else if (_playerClientData.Health.Value <= _playerClientData.MaxHealth.Value * 0.3f)
            {
                if (perkController!=null && perkController.IsEnabled(Perks.Fury))
                    Data.FuryMode.Value = true;
            }
            else if (Data.FuryMode)
            {
                if (_playerClientData.Health.Value > _playerClientData.MaxHealth.Value * 0.3f)
                {
                    Data.FuryMode.Value = false;
                }
            }
        }

		public int GetStrength()
		{
			return Data.Strength;
		}

		public void AfterAttackState()
		{
			ChangeState(_afterAttackState);
		}

		public void Update()
		{
			_playerClientData.Stamina.Value += _playerClientData.StaminaRegenaration * Time.deltaTime;
			_playerClientData.Stamina.Value = Mathf.Clamp(_playerClientData.Stamina.Value, 0, _playerClientData.MaxStamina);
		}

        public void TryHeal(int healValue)
        {
            _playerClientData.Health.Value = Math.Min(Data.MaxHealth, healValue + _playerClientData.Health.Value);
        }
    }
}