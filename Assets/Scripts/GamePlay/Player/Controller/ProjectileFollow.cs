﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileFollow : MonoBehaviour
{
	private Transform _targetTransform = null;
	void Start()
	{
		_targetTransform = transform.parent;
		transform.SetParent(null);
	}

	void FixedUpdate()
	{
		if (_targetTransform == null)
		{
			return;
		}
		transform.position = _targetTransform.position;
	}
}
