﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game
{
	public class Projectile : MonoBehaviour
	{
        [SerializeField] Projectile razrivShotProjectilePrefab;

		public Vector3 _direction = Vector3.zero;
		public float _speed = 0f;
		public int _damage = 0;
        public float _critChance = 0f;
        public LayerMask TargetLayer;
        public float _lifetime = 5f;
        public bool piercingShot = false;
        public bool ricochetShot = false;
        public bool razrivShot = false;
        public bool vampirism = false;
        private List<GameObject> lastRicochetTargets = null;
        public Collider ignoreThisTarget = null;

		public void SetDirection(Vector3 direction)
		{
			_direction = direction;
            transform.rotation = Quaternion.LookRotation(direction,Vector3.up);
		}

		public void Update()
		{
			transform.Translate(_speed * Time.deltaTime * _direction.normalized,Space.World);
            
            _lifetime -= Time.deltaTime;
            if (_lifetime < 0)
                Destroy(gameObject);
        }

		private void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.layer != TargetLayer || other == ignoreThisTarget)
			{
				return;
			}

			var controller = other.GetComponent<IController>();
			if (controller == null)
			{
                OnHitAnything(other);
                return;
			}

			if (controller.GetState() is RollState)
			{
				return;
			}

			var damageController = GetComponent<IDamageController>();
			if (damageController != null)
			{
                OnHitAnything(other);
                return;
			}

            var crit = false;
            var critDmg = _damage;
            if ((UnityEngine.Random.value + 0.1f) < _critChance)
            {
                critDmg *= 2;
                crit = true;
            }

            EventBus<DamageMessage>.Pub(new DamageMessage
			{
				DamageController = other.GetComponent<IDamageController>(),
				DamageValue = critDmg,
                target = other.transform,
                attacker = transform,
                crit = crit
            });

            if (vampirism && TargetLayer.value == LayerMask.NameToLayer("Enemy"))
            {
                EventBus<PlayerHealMessage>.Pub(new PlayerHealMessage
                {
                    healValue = Convert.ToInt32(Math.Ceiling(critDmg * 0.3f))
                });
            }

            OnHitAnything(other);

        }

        private class GameObjecstByClosest : IComparer<GameObject>
        {
            private GameObject from;
            public GameObjecstByClosest(GameObject from)
            {
                this.from = from;
            }

            public int Compare(GameObject x, GameObject y)
            {
                var xd = (x.transform.position - from.transform.position).magnitude;
                var yd = (y.transform.position - from.transform.position).magnitude;
                if (xd > yd)
                    return 1;
                else if (xd < yd)
                    return 1;
                else
                    return 0;
            }
        }

        private void OnHitAnything(Collider hit)
        {
            if (razrivShot)
            {
                var dirs = SpreadShotHelper.SpreadDirections(_direction, 3, 30);
                foreach (var dir in dirs)
                {
                    var newProjectile = Instantiate(razrivShotProjectilePrefab);
                    newProjectile.transform.position = transform.position;
                    newProjectile.transform.rotation = transform.rotation;
                    newProjectile.SetDirection(dir);
                    newProjectile.TargetLayer = TargetLayer;
                    newProjectile.piercingShot = piercingShot;
                    newProjectile.ricochetShot = ricochetShot;
                    newProjectile.vampirism = vampirism;
                    newProjectile.razrivShot = false;
                    newProjectile._critChance = _critChance;
                    newProjectile.ignoreThisTarget = hit;
                    newProjectile._critChance = _critChance;
                    newProjectile._damage = _damage; 
                    newProjectile._speed = _speed;
                    newProjectile._lifetime = 5f;
                }
                Destroy(gameObject);
            }
            else if (ricochetShot)
            {
                _lifetime = 5f;
                if (lastRicochetTargets == null)
                    lastRicochetTargets = new List<GameObject>();
                var enemies = FindGameObjectsByLayer.Find(TargetLayer).Where(x => !lastRicochetTargets.Contains(x)).ToList();
                if (enemies.Count() > 0)
                {
                    ignoreThisTarget = hit;
                    enemies.Sort(new GameObjecstByClosest(gameObject));
                    var closest = enemies.First();
                    lastRicochetTargets.Add(closest);
                    var direction = (closest.transform.position - transform.position).normalized;
                    direction.y = 0;
                    SetDirection(direction);
                    _damage = Convert.ToInt32(Math.Ceiling(_damage * 0.1f));
                }
            }
            else if (!piercingShot)
                Destroy(gameObject);
        }
	}
}