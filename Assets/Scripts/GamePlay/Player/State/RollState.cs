﻿using UnityEngine;

namespace Game
{
	public class RollState : PlayerBaseState
	{
        public Animator Animator = null;

        public override void StateEnter()
        {
			print("Roll");
            Animator.SetTrigger("Roll");
        }

        public override void Damage(int damage)
		{
		}
	}
}