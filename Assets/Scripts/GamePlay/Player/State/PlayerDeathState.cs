﻿using Game.Dungeon;
using UnityEngine;

namespace Game
{
	public class PlayerDeathState : DeathState
	{
		
		public override void StateEnter()
		{
			var pc = PlayerController.Instance;
			EventBus<PlayerDeathMessage>.Pub(new PlayerDeathMessage() { Health = pc.Health, Attack = pc.Attack, Stamina = pc.Stamina });
			Animator.SetTrigger("Death");
			Animator.SetFloat("SpeedMultiply", 1.0f);
			gameObject.layer = LayerMask.NameToLayer("Default");
		}
	}
}