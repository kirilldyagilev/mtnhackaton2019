﻿using System;
using UnityEngine;

namespace Game
{
	public class LiveState : PlayerBaseState 
	{
		private Rigidbody _rigidBody = null;
		private PlayerController _controller = null;

		[SerializeField] private State _attackState = null;
		[SerializeField] private State _rollState = null;
		[SerializeField] private IAttacker _attacker = null;
        public Animator Animator = null;
        private PerkController perkController = null;

        private void Awake()
		{
			_rigidBody = GetComponent<Rigidbody>();
			_attacker = GetComponent<IAttacker>();
			_controller = GetComponent<PlayerController>();
		}
		
		public override void Attack(Vector3 attackDirection)
		{
            if (PlayerController.Instance.Data.Stamina < PlayerController.Instance.Data.AttackCost)
            {
				_controller.OnStaminaNeeded?.Invoke();
                return;
            }
            Animator.SetBool("IsRun", false);
			_controller.ChangeState(_attackState);
            PlayerController.Instance.Data.Stamina.Value -= PlayerController.Instance.Data.AttackCost;

            var atkSpd = 1.0f;

            if (perkController != null)
            {
                if (perkController.IsEnabled(Perks.AttackSpeedBoost1))
                    atkSpd = 1.2f;
                if (perkController.IsEnabled(Perks.AttackSpeedBoost2))
                    atkSpd = 1.5f;
            }

            if (PlayerController.Instance.Data.FuryMode)
                atkSpd *= 1.5f;

            _attacker.SetAttackSpeed(atkSpd);

            Invoke(nameof(AfterAttackDelay), PlayerController.Instance.Data.AttackDelay * (1.0f / atkSpd));
            _attacker.StartAttack(attackDirection, LayerMask.NameToLayer("Enemy"));
            _controller.transform.forward = attackDirection;

        }

		private void AfterAttackDelay()
		{
			_controller.CheckState();
		}

		public override void Move(Vector3 direction, float speed)
		{
            if (Mathf.Approximately(direction.sqrMagnitude, 0))
            {
                Animator.SetBool("IsRun", false);
                return;
            }
            Animator.SetBool("IsRun", true);
            var newPosition = transform.position + PlayerController.Instance.Data.Speed.Value * Time.deltaTime * direction;
            _rigidBody.MovePosition(newPosition);

            var newRotation = Quaternion.LookRotation(direction, Vector3.up);
            _rigidBody.MoveRotation(newRotation);
			
			print("Move player");
			print(transform.position);
			print(PlayerController.Instance.Data.Speed.Value);
			print(Time.deltaTime);
			print(direction);
		}

		public override void Roll(Vector3 rollDirection)
		{
			if (PlayerController.Instance.Data.Stamina < PlayerController.Instance.Data.RollCost)
			{
				_controller.OnStaminaNeeded?.Invoke();
				return;
			}

			_controller.ChangeState(_rollState);
			PlayerController.Instance.Data.Stamina.Value -= PlayerController.Instance.Data.RollCost;
			_controller.Roll(rollDirection);
		}

		public override void StateExit(State newState)
		{
			if (newState != _attackState)
			{
				CancelInvoke(nameof(AfterAttackDelay));
			}
		}
	}
}