﻿using UnityEngine;

namespace Game
{
	public class PlayerBaseState : State
	{
		public override void Damage(int damage)
		{
			PlayerController.Instance.SetDamage(damage);
		}
	}
}