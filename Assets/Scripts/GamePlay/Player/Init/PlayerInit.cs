﻿using System;
using UnityEngine;

namespace Game
{
	public class PlayerInit : MonoBehaviour
	{
		[SerializeField] private PlayerData _playerData = null;
        private PerkController perkController = null;
		
		public void Awake()
		{
			var playerClientData = PlayerController.Instance.Data;
            perkController = GetComponent<PerkController>();
			var profile = ProfileOchestrator.CurrentProfileData;

			playerClientData.AttackDelay.Value = _playerData.AttackDelay;

            var healthMod = 1.0f;
            if (perkController != null && perkController.IsEnabled(Perks.MaxHPIncrease))
                healthMod = 1.2f;
			playerClientData.MaxHealth.Value = Convert.ToInt32(Math.Ceiling(profile.HP * healthMod));
            playerClientData.Health.Value = ProfileOchestrator.CurrentHP;

            playerClientData.Speed.Value = _playerData.Speed;
			playerClientData.Stamina.Value = _playerData.Stamina;
			playerClientData.RollCost.Value = _playerData.RollCost;
			playerClientData.MaxStamina.Value = _playerData.Stamina;
			playerClientData.StaminaRegenaration.Value = _playerData.StaminaRegeneration;
			playerClientData.Strength.Value = profile.Attack;
			playerClientData.RollSpeed.Value = _playerData.RollSpeed;
			playerClientData.RollDuration.Value = _playerData.RollDuration;
            playerClientData.AttackCost.Value = _playerData.AttackCost;
			playerClientData.SpeedWhenReloadCoeff.Value = _playerData.SpeedWhenReloadCoeff;
			GetComponent<Attacker>().Weapon.SetDamage(profile.Attack);
		}
	}
}