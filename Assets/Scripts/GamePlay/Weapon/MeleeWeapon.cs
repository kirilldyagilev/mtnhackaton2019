﻿using System;

using Game;

using UnityEngine;

namespace GamePlay.Weapon
{
	public class MeleeWeapon : BaseWeapon
	{
		private bool _isAttack = false;

		public float _attackDelay;

		public override void StartFire(Vector3 direction, LayerMask targetMask)
		{
			_direction = direction;
			_targetLayer = targetMask;

			if (_characterAnimator != null)
			{
				_characterAnimator.SetTrigger(_characterAttackTrigger);
			}

			if (_weaponAnimator != null)
			{
				_weaponAnimator.SetTrigger(_weaponAttackTrigger);
			}
			
			//Invoke(nameof(Fire), _attackDelay);
			_isAttack = false;
		}

		public override void Fire()
		{
			_isAttack = true;
			print("Fire");

            SoundSystem.PlaySound(
                new AudioClip[] { Sounds.instance.snd_enemy_warrior_attack1, Sounds.instance.snd_enemy_warrior_attack2, Sounds.instance.snd_enemy_warrior_attack3 },
                transform.position
            );
        }

		public override void StopFire()
		{
			_isAttack = false;
			print("StopFire");
		}

		private void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.layer != _targetLayer || !_isAttack)
			{
				return;
			}

			var controller = other.GetComponent<IController>();
			if (controller == null)
			{
				return;
			}

			if (controller.GetState() is RollState)
			{
				return;
			}

			var damageController = GetComponent<IDamageController>();
			if (damageController != null)
			{
				return;
			}

			EventBus<DamageMessage>.Pub(new DamageMessage
			{
				DamageController = other.GetComponent<IDamageController>(),
				DamageValue = _damage,
				target = other.transform,
				attacker = transform
			});

			_isAttack = false;
		}
	}
}