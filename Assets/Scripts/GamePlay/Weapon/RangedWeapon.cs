﻿using System;
using System.Collections;

using GamePlay.Weapon;

using UnityEngine;

namespace Game
{
	public class RangedWeapon : BaseWeapon
	{
		[SerializeField] private Projectile _projectilePrefab = null;
		[SerializeField] private Transform _spawner = null;

		public ParticleSystem Effect = null;

		public override void StartFire(Vector3 direction, LayerMask targetMask)
		{
			_direction = direction;
			_targetLayer = targetMask;

			if (_characterAnimator != null)
			{
				_characterAnimator.SetTrigger(_characterAttackTrigger);
			}

			if (_weaponAnimator != null)
			{
				_weaponAnimator.SetTrigger(_weaponAttackTrigger);
			}
		}

		public override void Fire()
		{
            if (perkController!=null && perkController.IsEnabled(Perks.MultiShot))
            {
                var dirs = SpreadShotHelper.SpreadDirections(_direction, 2, 10);
                foreach (var dir in dirs)
                {
                    ShootProjectile(dir);
                }
            } else
                ShootProjectile(_direction);

            if (perkController != null)
            {
                SoundSystem.PlaySound(
                    new AudioClip[] {
                        Sounds.instance.snd_player_fire1,
                        Sounds.instance.snd_player_fire2,
                        Sounds.instance.snd_player_fire3,
                        Sounds.instance.snd_player_fire4
                    },
                    transform.position
                );
            } else
                SoundSystem.PlaySound(
                    new AudioClip[] { Sounds.instance.snd_enemy_shooter_attack1, Sounds.instance.snd_enemy_shooter_attack2, Sounds.instance.snd_enemy_shooter_attack3 },
                    transform.position
                );
        }

        private void ShootProjectile(Vector3 _dir)
        {
			if (Effect != null)
			{
				Effect.Play();
			}

			var newProjectile = Instantiate(_projectilePrefab);
            newProjectile.transform.position = _spawner.position;
            newProjectile.SetDirection(_dir);
            newProjectile.TargetLayer = _targetLayer;
            newProjectile._damage = _damage;

            if (perkController != null)
            {
                newProjectile.piercingShot = perkController.IsEnabled(Perks.PiercingShot);
                newProjectile.razrivShot = perkController.IsEnabled(Perks.RazrivShot);
                newProjectile.ricochetShot = perkController.IsEnabled(Perks.RicochetShot);
                newProjectile._critChance = 0f;
                newProjectile._critChance = perkController.IsEnabled(Perks.CritMaster1) ? 0.2f : newProjectile._critChance;
                newProjectile._critChance = perkController.IsEnabled(Perks.CritMaster2) ? 0.4f : newProjectile._critChance;
                newProjectile._critChance = perkController.IsEnabled(Perks.CritMaster3) ? 0.6f : newProjectile._critChance;
                newProjectile.vampirism = perkController.IsEnabled(Perks.Vampirism);

                float dmgModifier = 1;
                if (perkController.IsEnabled(Perks.AttackBoost1))
                    dmgModifier = 1.5f;
                if (perkController.IsEnabled(Perks.AttackBoost2))
                    dmgModifier = 2.5f;
                if (perkController.IsEnabled(Perks.AttackBoost3))
                    dmgModifier = 3.0f;
                newProjectile._damage = Convert.ToInt32(Mathf.Ceil(_damage * dmgModifier));
            }
        }
	}
}
