﻿using Game;
using UnityEngine;

namespace GamePlay.Weapon
{
	public class BaseWeapon : MonoBehaviour
	{
		[SerializeField] protected Animator _characterAnimator = null;
		[SerializeField] protected Animator _weaponAnimator = null;
        [SerializeField] protected PerkController perkController = null;
        [SerializeField] protected string _characterAttackTrigger = null;
		[SerializeField] protected string _weaponAttackTrigger = null;
		[SerializeField] protected int _damage = 0;

		protected Vector3 _direction;
		protected LayerMask _targetLayer;

        protected virtual void Awake()
        {
            perkController = GetComponent<PerkController>();
        }

        public void SetDamage(int damage)
		{
			_damage = damage;
		}
		public virtual void StartFire(Vector3 direction, LayerMask layerMask)
		{
		}

		public virtual void Fire()
		{
		}

		public virtual void StopFire()
		{
		}
	}
}