﻿using Game;

using UnityEngine;

namespace GamePlay.Weapon
{
	public class MagicStuff : BaseWeapon
	{
		[SerializeField] private Projectile _projectilePrefab = null;
		[SerializeField] private Transform _spawner = null;
		public int ProjectileCount;
		public int AngleBetween;
		
		public override void StartFire(Vector3 direction, LayerMask targetMask)
		{
			_direction = direction;
			_targetLayer = targetMask;

			if (_characterAnimator != null)
			{
				_characterAnimator.SetTrigger(_characterAttackTrigger);
			}

			if (_weaponAnimator != null)
			{
				_weaponAnimator.SetTrigger(_weaponAttackTrigger);
			}
		}

		public override void Fire()
		{

            if (ProjectileCount >= 9)
                SoundSystem.PlaySound(Sounds.instance.snd_enemy_mag_attack3, transform.position);
            else if (ProjectileCount >= 4)
                SoundSystem.PlaySound(Sounds.instance.snd_enemy_mag_attack2, transform.position);
            else 
                SoundSystem.PlaySound(Sounds.instance.snd_enemy_mag_attack1, transform.position);

            var randomDir = Random.insideUnitCircle.normalized;
            var newDir = new Vector3(randomDir.x, 0, randomDir.y);
            //var newDir = _direction; newDir.y = 0;
            var dirs = SpreadShotHelper.SpreadDirections(newDir, ProjectileCount, AngleBetween);
			foreach (var dir in dirs)
			{
                ShootProjectile(dir);
			}
		}
		
		private void ShootProjectile(Vector3 _dir)
		{
			var newProjectile = Instantiate(_projectilePrefab);
			newProjectile.transform.position = _spawner.position;
			newProjectile.SetDirection(_dir);
			newProjectile.TargetLayer = _targetLayer;
			newProjectile._damage = _damage;
		}
	}
}