﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game
{
	public class MageMobController : BaseMobController
	{
        public override void SetDamage(int damage)
        {
            base.SetDamage(damage);

            SoundSystem.PlaySound(
                new AudioClip[] { Sounds.instance.snd_enemy_mag_hit1, Sounds.instance.snd_enemy_mag_hit2, Sounds.instance.snd_enemy_mag_hit3 }, 
                transform.position
            );
        }
    }
}
