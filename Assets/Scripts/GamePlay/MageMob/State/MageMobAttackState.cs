﻿using Game.Archer;
using UnityEngine;
using UnityEngine.AI;

namespace Game
{
    public class MageMobAttackState : DieableState
    {
        private NavMeshAgent _navMeshAgent;

        private void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        public override void StateEnter()
        {
            _navMeshAgent.SetDestination(transform.position);
        }

        public override void MoveToTarget(Vector3 target)
        {
            _navMeshAgent.SetDestination(transform.position);
        }
    }
}