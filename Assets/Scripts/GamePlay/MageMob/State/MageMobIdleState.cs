﻿using Game.Archer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GamePlay.Weapon;

using UnityEngine;
using UnityEngine.AI;

namespace Game
{
    [RequireComponent(typeof(NavMeshAgent))]
    class MageMobIdleState : DieableState
    {
        [SerializeField] private State _attackState = null;
        private IAttacker _attacker = null;
        private MageMobController _controller;
        private NavMeshAgent _navMeshAgent;
        private Vector3 previousTargetPosition = Vector3.zero;
        public Animator Animator;
        public Notifier Notifier;
        public int tierId;

        private void Awake()
        {
            _attacker = GetComponent<IAttacker>();
            _controller = GetComponent<MageMobController>();
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        public override void Attack(Vector3 attackDirection)
        {
            _controller.ChangeState(_attackState);
            Invoke(nameof(AfterAttackDelay), _controller.Data.AttackDelay);

            var direction = (PlayerController.Instance.transform.position - transform.position).normalized;
            
            (_attacker.GetWeapon() as MagicStuff).AngleBetween = (_controller.BaseData as MagEnemyData).spreadAngle;
            (_attacker.GetWeapon() as MagicStuff).ProjectileCount = (_controller.BaseData as MagEnemyData).projectiles;
            _attacker.StartAttack(direction, LayerMask.NameToLayer("Player"));
            _attacker.SetAttackSpeed(_controller.Data.AttackSpeed);
            //_attacker.StartAttack(leftDirection, LayerMask.NameToLayer("Player"));
            Animator.SetTrigger("Shoot");
            Notifier?.Notify(tierId);
        }

        public override void Move(Vector3 directon, float speed)
        {
            transform.position += directon * speed * Time.deltaTime;
            Quaternion targetRotation = Quaternion.LookRotation(directon);
            targetRotation.z = 0;
            targetRotation.x = 0;
            transform.rotation = targetRotation;
        }

        public override void MoveToTarget(Vector3 target)
        {
            if (Vector3.SqrMagnitude(previousTargetPosition - target) > 0.1f)
            {
                _navMeshAgent.stoppingDistance = 1f;
                _navMeshAgent.speed = _controller.Data.Speed;
                _navMeshAgent.SetDestination(target);
                previousTargetPosition = target;
                Animator.SetTrigger("Run");
            }
        }

        private void AfterAttackDelay()
        {
            if (_controller.GetState() == _attackState)
                _controller.ChangeState(this);
        }

    }
}
