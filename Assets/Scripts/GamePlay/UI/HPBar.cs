﻿using System;
using System.Collections;
using System.Collections.Generic;

using Game;

using GamePlay.UI;

using UnityEngine;
using UnityEngine.UI;

public class HPBar : Bar
{
	private void Start()
	{
		PlayerController.Instance.Data.Health.OnChanged += HealthChangedHandler;
		
		Initialize((float) PlayerController.Instance.Data.Health / PlayerController.Instance.Data.MaxHealth);
	}

	private void OnDestroy()
	{
		if (PlayerController.Instance != null)
		{
			PlayerController.Instance.Data.Health.OnChanged -= HealthChangedHandler;
		}
	}

	private void HealthChangedHandler(int oldValue, int newValue)
	{
		SetBar((float )newValue / PlayerController.Instance.Data.MaxHealth);
	}
}
