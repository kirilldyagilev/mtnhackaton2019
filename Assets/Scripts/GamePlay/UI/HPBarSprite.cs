﻿using Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPBarSprite : AlwaysFaceCamera
{
	[SerializeField] private Transform bar = null;
    [SerializeField] private SpriteRenderer barColor = null;

    public void SetSize(float sizeNormalized)
	{
		bar.localScale = new Vector3(sizeNormalized, 1f);
	}

	public void SetColor(Color color)
	{
        barColor.color = color;
	}

}
