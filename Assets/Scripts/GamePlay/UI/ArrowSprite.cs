﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowSprite : MonoBehaviour
{
    public void SetTarget(Transform target)
    {
        Vector3 targetPosition = target.position;
        Quaternion targetRotation = Quaternion.LookRotation(targetPosition - transform.position);
        targetRotation.z = 0;
        targetRotation.x = 0;
        transform.rotation = targetRotation;

    }

    public void SetAngle(float degrees)
    {
        var rot = transform.rotation;
        rot.y = degrees;
        transform.rotation = rot;
    }

}
