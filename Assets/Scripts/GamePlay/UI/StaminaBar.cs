﻿using Game;

using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

namespace GamePlay.UI
{
	public class StaminaBar : Bar
	{
		[SerializeField] private Image _staminaNeeded = null;
		[SerializeField] private float _shiningDuration = 0f;

		private bool _isShining = false;
		
		private void Start()
		{
			PlayerController.Instance.Data.Stamina.OnChanged += StaminaChangedHandler;
			PlayerController.Instance.OnStaminaNeeded += StaminaNeededHandler;
		
			Initialize((float) PlayerController.Instance.Data.Stamina / PlayerController.Instance.Data.MaxStamina);
		}

		private void OnDestroy()
		{
			if (PlayerController.Instance != null)
			{
				PlayerController.Instance.Data.Stamina.OnChanged -= StaminaChangedHandler;
				PlayerController.Instance.OnStaminaNeeded -= StaminaNeededHandler;
			}
		}

		private void StaminaChangedHandler(float oldValue, float newValue)
		{
			SetBar(newValue / PlayerController.Instance.Data.MaxStamina);
		}

		private void StaminaNeededHandler()
		{
			if (_isShining)
			{
				return;
			}

			_isShining = true;
			_staminaNeeded.DOColor(Color.white, _shiningDuration).SetLoops(2, LoopType.Yoyo).OnComplete(() => _isShining = false);
		}
		
	}
}