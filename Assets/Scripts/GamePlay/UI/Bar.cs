﻿using UnityEngine;
using UnityEngine.UI;

namespace GamePlay.UI
{
	public class Bar : MonoBehaviour
	{
		[SerializeField] private Image _barImage = null;
		[SerializeField] protected float _fillAmountCoeff = 0f;
		[SerializeField] protected float _barSpeed = 0f;

		public void Initialize(float coeff)
		{
			_fillAmountCoeff = coeff;
			_barImage.fillAmount = _fillAmountCoeff;
		}
		
		public void SetBar(float newValue)
		{
			_fillAmountCoeff = newValue;
		}
		
		private void FixedUpdate()
		{
			_barImage.fillAmount = Mathf.Lerp(_barImage.fillAmount, _fillAmountCoeff, _barSpeed * Time.fixedDeltaTime);
		}
	}
}