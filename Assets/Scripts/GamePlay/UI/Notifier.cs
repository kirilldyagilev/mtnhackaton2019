﻿using System;

using UnityEngine;

namespace Game
{
	public class Notifier : AlwaysFaceCamera
	{
		public SpriteRenderer _spriteTier1;
		public SpriteRenderer _spriteTier2;
		public SpriteRenderer _spriteTier3;

		public float _showTime = 0f;
		private Transform _cameraTransform = null;

		public void Notify(int tierId)
		{
			switch (tierId)
			{
				case 0:
					if (_spriteTier1 != null)
					{
						_spriteTier1.gameObject.SetActive(true);
					}
					break;
				
				case 1:
					if (_spriteTier2 != null)
					{
						_spriteTier2.gameObject.SetActive(true);
					}
					break;
				
				case 2:
					if (_spriteTier3 != null)
					{
						_spriteTier3.gameObject.SetActive(true);
					}
					break;
			}
			
			CancelInvoke(nameof(HideShowTime));
			Invoke(nameof(HideShowTime), _showTime);
		}

		public void HideShowTime()
		{
			_spriteTier1.gameObject.SetActive(false);
			_spriteTier2.gameObject.SetActive(false);
			_spriteTier3.gameObject.SetActive(false);
		}
	}
}