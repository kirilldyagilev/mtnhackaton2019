﻿using Game;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GamePlay.UI
{
    [RequireComponent(typeof(PerkController))]
    public class PerksPanel : MonoBehaviour
    {
        [SerializeField] private PerksTableData perksTableData;
        private PerkController perkController;

        private List<PerkIcon> icons;

        private class PerkIcon
        {
            public string id;
            public Image icon;
        }

        private void Awake()
        {
            perkController = GetComponent<PerkController>();
            icons = new List<PerkIcon>();

            if (perkController != null)
            {
                perkController.activated.ForEach(x => EnablePerkIcon(x, true));
                perkController.OnPerksChanged += EnablePerkIcon;
                perkController.OnPerksLoaded += OnPerksLoaded;
            }
        }

        private void OnPerksLoaded()
        {
            Clear();
            perkController.activated.ForEach(x => EnablePerkIcon(x, true));
        }

        private void OnDisable()
        {
            if (perkController != null)
            {
                perkController.OnPerksChanged -= EnablePerkIcon;
                perkController.OnPerksLoaded -= OnPerksLoaded;
            }
        }

        private void Clear()
        {
            icons.ForEach(x => Destroy(x.icon));
            icons = new List<PerkIcon>();
        }

        private void EnablePerkIcon(string perk, bool enabled)
        {
            var perkIcon = icons.Find(x=>x.id == perk);
            var perkData = perksTableData.perks.Find(x => x.key == perk);

            if (enabled)
            {
                if (perkData != null && perkIcon == null)
                {
                    var obj = Instantiate(perkData.icon);
                    obj.transform.SetParent(transform, false);
                    icons.Add(new PerkIcon() { id = perk, icon = obj } );
                } else
                {
                    Debug.LogError("perk icon is not set in perks table!");
                }
            } else
            {
                if (perkIcon != null)
                {
                    icons.Remove(perkIcon);
                    Destroy(perkIcon.icon);
                }
            }
             
            

        }

    }
}