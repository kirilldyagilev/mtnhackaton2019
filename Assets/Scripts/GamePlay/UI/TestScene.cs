﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScene : MonoBehaviour
{
    [SerializeField]
    private Transform player;
    [SerializeField]
    private Transform enemy;
    [SerializeField]
    private ArrowSprite enemyArrow;

    void Start()
    {
        
        //enemyArrow.SetAngle(90);
    }

    void Update()
    {
        enemyArrow.SetTarget(player);
    }
}
