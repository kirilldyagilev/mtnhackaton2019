﻿using System.Collections;

using Game;

using UnityEngine;

namespace GamePlay.BossMelee.State
{
	public class BossState1Attack : BaseBossState
	{
		public override void Attack(Vector3 attackDirection)
		{
			StopAllCoroutines();
			StartCoroutine(Attack1Coroutine());
		}
		
		protected IEnumerator Attack1Coroutine()
		{
			print("Attack 1");
			
			_controller.ChangeState(_attackState);
			
			_attacker.StartAttack((PlayerController.Instance.transform.position - transform.position).normalized, LayerMask.NameToLayer("Player"));
			_attacker.SetAttackSpeed(_shootTime);
			
			yield return new WaitForSeconds(_controller.GetAfterShootDelay());
			
			_controller.CheckState();
		}
	}
}