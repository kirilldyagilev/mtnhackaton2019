﻿using Game;
using UnityEngine;

namespace GamePlay.BossMelee
{
	public class BossMeleeController : MeleeMobController
	{
        public override void SetDamage(int damage)
        {
            base.SetDamage(damage);

            SoundSystem.PlaySound(
                new AudioClip[] { Sounds.instance.snd_enemy_warrior_hit1, Sounds.instance.snd_enemy_warrior_hit2, Sounds.instance.snd_enemy_warrior_hit3 },
                transform.position
            );
        }
    }
}