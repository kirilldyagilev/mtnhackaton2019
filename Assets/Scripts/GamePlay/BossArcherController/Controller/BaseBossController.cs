﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game
{
    public class BaseBossController: BaseMobController
    {
        [SerializeField] private State _boss1State;
        [SerializeField] private State _boss2State;
        [SerializeField] private State _boss3State;

        private BossData _bossData;

        protected override void Awake()
        {
            base.Awake();

            _bossData = _data as BossData;
            Data.Health.OnChanged += HealthChangedHandler;
        }

        private void OnDestroy()
        {
            Data.Health.OnChanged -= HealthChangedHandler;
        }

        private void HealthChangedHandler(int oldValue, int newValue)
        {
            var currentState = GetCurrentState();
            
            if (_bossData.StateSpeed.Length > currentState)
            {
                Data.Speed.Value = _bossData.StateSpeed[currentState];
            }
        }

        private int GetCurrentState()
        {
            for (int i = 0; i < _bossData.StateHealth.Length; i++)
            {
                if (Data.Health >= _bossData.StateHealth[i])
                {
                    return i;
                }
            }

            return _bossData.StateHealth.Length - 1;
        }

        public override void CheckState()
        {
            var currentState = GetCurrentState();

            switch (currentState)
            {
                case 0:
                    _idleState = _boss1State;
                    break;
                case 1:
                    _idleState = _boss2State;
                    break;
                case 2:
                    _idleState = _boss3State;
                    break;
            }

            ChangeState(Data.Health <= 0f ? _deathState : _idleState);
        }

        public float GetAfterShootDelay()
        {
            return _bossData.StateAttackDelay[GetCurrentState()];
        }
    }
}
