﻿using GamePlay.Weapon;
using System.Collections;

using UnityEngine;

namespace Game.BossArcher
{
	public class BossState1Attack : BaseBossState
	{
		public override void Attack(Vector3 attackDirection)
		{
			StopAllCoroutines();
			StartCoroutine(Attack1Coroutine());
		}
		
		protected IEnumerator Attack1Coroutine()
		{
			print("Attack 1");
			
			Notifier?.Notify(0);
			
			_controller.ChangeState(_attackState);

            var dir = (PlayerController.Instance.transform.position - transform.position);
            dir.y = 0;

            if (_attacker.GetWeapon() is MagicStuff && (_controller.BaseData as BossMageData))
            {
                (_attacker.GetWeapon() as MagicStuff).AngleBetween = (_controller.BaseData as BossMageData).ProjectileSpread[0];
                (_attacker.GetWeapon() as MagicStuff).ProjectileCount = (_controller.BaseData as BossMageData).ProjectileNum[0];
            }

            _attacker.StartAttack(dir.normalized, LayerMask.NameToLayer("Player"));
			_attacker.SetAttackSpeed(_shootTime);
			Animator.SetTrigger("Shoot");
			
			yield return new WaitForSeconds(_controller.GetAfterShootDelay());
			
			_controller.CheckState();
		}
	}
}