﻿using System.Collections;

using Game.Archer;

using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Playables;

namespace Game.BossArcher
{
	[RequireComponent(typeof(NavMeshAgent))]
	public class BaseBossState : DieableState
	{
		protected IAttacker _attacker = null;
		protected BaseBossController _controller = null;
		private Rigidbody _rigidBody = null;
		private NavMeshAgent _navMeshAgent;
		private Vector3 _previousTargetPosition = Vector3.zero;

		public Archer.AttackState _attackState = null;

		public Animator Animator;

		public float _delayBetweenShoots = 0f;
		public float _shootTime = 1f;
		public Notifier Notifier;

		private void Awake()
		{
			_attacker = GetComponent<IAttacker>();
			_rigidBody = GetComponent<Rigidbody>();
			_controller = GetComponent<BaseBossController>();
			_navMeshAgent = GetComponent<NavMeshAgent>();
		}

		public override void Attack(Vector3 attackDirection)
		{
		}

		public override void Move(Vector3 direction, float speed)
		{
			if (Mathf.Approximately(direction.sqrMagnitude, 0))
			{
				return;
			}
			var newPosition = transform.position + speed * Time.deltaTime * direction;
			_rigidBody.MovePosition(newPosition);

			var newRotation = Quaternion.LookRotation(direction, Vector3.up);
			_rigidBody.MoveRotation(newRotation);
		}

		public override void MoveToTarget(Vector3 target)
		{
			if (Vector3.SqrMagnitude(_previousTargetPosition - target) > 0.1f)
			{
				_navMeshAgent.stoppingDistance = 1f;
				_navMeshAgent.speed = _controller.Data.Speed;
				_navMeshAgent.SetDestination(target);
				_previousTargetPosition = target;
				Animator.SetBool("Run", true);
			}
		}
		
		protected void AfterAttackDelay()
		{
			_controller.CheckState();
		}
	}
}