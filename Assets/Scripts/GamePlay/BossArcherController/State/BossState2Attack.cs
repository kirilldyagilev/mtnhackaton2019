﻿using GamePlay.Weapon;
using System.Collections;

using UnityEngine;

namespace Game.BossArcher
{
	public class BossState2Attack : BossState1Attack
	{
		public override void Attack(Vector3 attackDirection)
		{
			StopAllCoroutines();
			
			var random = new System.Random();
			var randomIndex = random.Next(0, 2);

			switch (randomIndex)
			{
				case 0:
					StartCoroutine(Attack1Coroutine());
					break;
				case 1:
					StartCoroutine(Attack2Coroutine());
					break;
			}
		}
		
		protected IEnumerator Attack2Coroutine()
		{
			print("Attack 2");
			
			Notifier?.Notify(1);
			
			_controller.ChangeState(_attackState);

            var dir = (PlayerController.Instance.transform.position - transform.position);
            dir.y = 0;

            if (_attacker.GetWeapon() is MagicStuff && (_controller.BaseData as BossMageData))
            {
                (_attacker.GetWeapon() as MagicStuff).AngleBetween = (_controller.BaseData as BossMageData).ProjectileSpread[1];
                (_attacker.GetWeapon() as MagicStuff).ProjectileCount = (_controller.BaseData as BossMageData).ProjectileNum[1];
            }

            _attacker.StartAttack(dir.normalized, LayerMask.NameToLayer("Player"));
			_attacker.SetAttackSpeed(_shootTime);
			Animator.SetTrigger("Shoot");
			
			yield return new WaitForSeconds(_delayBetweenShoots);
			if (_controller.Data.Health <= 0)
			{
				yield break;
			}
			
			dir = (PlayerController.Instance.transform.position - transform.position);
			dir.y = 0;
			_attacker.StartAttack(dir.normalized, LayerMask.NameToLayer("Player"));
			Animator.SetTrigger("Shoot");
			
			yield return new WaitForSeconds(_controller.GetAfterShootDelay());
			
			_controller.CheckState();
		}
	}
}