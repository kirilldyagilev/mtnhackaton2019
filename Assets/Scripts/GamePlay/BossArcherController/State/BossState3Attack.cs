﻿using GamePlay.Weapon;
using System.Collections;

using UnityEngine;

namespace Game.BossArcher
{
	public class BossState3Attack : BossState2Attack
	{
		public override void Attack(Vector3 attackDirection)
		{
			StopAllCoroutines();

			var random = new System.Random();
			var randomIndex = random.Next(0, 3);

			switch (randomIndex)
			{
				case 0:
					StartCoroutine(Attack1Coroutine());
					break;
				case 1:
					StartCoroutine(Attack2Coroutine());
					break;
				case 2:
					StartCoroutine(Attack3Coroutine());
					break;
			}
		}

		private IEnumerator Attack3Coroutine()
		{
			print("Attack 3");
			
			Notifier?.Notify(2);
			
			_controller.ChangeState(_attackState);

            var dir = (PlayerController.Instance.transform.position - transform.position);
            dir.y = 0;

            if (_attacker.GetWeapon() is MagicStuff && (_controller.BaseData as BossMageData))
            {
                (_attacker.GetWeapon() as MagicStuff).AngleBetween = (_controller.BaseData as BossMageData).ProjectileSpread[2];
                (_attacker.GetWeapon() as MagicStuff).ProjectileCount = (_controller.BaseData as BossMageData).ProjectileNum[2];
            }

            _attacker.StartAttack(dir.normalized, LayerMask.NameToLayer("Player"));
			_attacker.SetAttackSpeed(_shootTime);
			Animator.SetTrigger("Shoot");
			
			yield return new WaitForSeconds(_delayBetweenShoots);
			if (_controller.Data.Health <= 0)
			{
				yield break;
			}
			
			dir = (PlayerController.Instance.transform.position - transform.position);
			dir.y = 0;
			_attacker.StartAttack(dir.normalized, LayerMask.NameToLayer("Player"));
			Animator.SetTrigger("Shoot");
			
			yield return new WaitForSeconds(_delayBetweenShoots);
			if (_controller.Data.Health <= 0)
			{
				yield break;
			}
			
			dir = (PlayerController.Instance.transform.position - transform.position);
			dir.y = 0;
			_attacker.StartAttack(dir.normalized, LayerMask.NameToLayer("Player"));
			Animator.SetTrigger("Shoot");
			
			yield return new WaitForSeconds(_controller.GetAfterShootDelay());
			
			_controller.CheckState();
		}
	}
}