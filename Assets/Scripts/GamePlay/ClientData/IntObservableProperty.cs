﻿using System;
using UnityEngine;

namespace Game
{
	[Serializable]
	public class IntObservableProperty
	{
		[SerializeField]
		private int value = default;
		public Action<int, int> OnChanged = null;
		public int Value
		{
			get
			{
				return value;
			}
			set
			{
				if (this.value != value)
				{
					var oldValue = this.value;
					var newValue = value;
					this.value = value;
					OnChanged?.Invoke(oldValue, newValue);
				}
			}
		}

		public static implicit operator int(IntObservableProperty i) => i.Value;

	}
}