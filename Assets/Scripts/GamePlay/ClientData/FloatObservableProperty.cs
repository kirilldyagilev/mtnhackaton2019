﻿using System;

using UnityEngine;

namespace Game
{
	[Serializable]
	public class FloatObservableProperty
	{
		[SerializeField]
		private float value = default;
		public Action<float, float> OnChanged = null;
		public float Value
		{
			get
			{
				return value;
			}
			set
			{
				if (this.value != value)
				{
					var oldValue = this.value;
					var newValue = value;
					this.value = value;
					OnChanged?.Invoke(oldValue, newValue);
				}
			}
		}

		public static implicit operator float(FloatObservableProperty i) => i.Value;
	}
}