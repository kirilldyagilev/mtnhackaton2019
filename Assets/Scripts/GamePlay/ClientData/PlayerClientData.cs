﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace Game
{
	[Serializable]
	public class PlayerClientData : MobClientData
	{
		public FloatObservableProperty Stamina;
		public FloatObservableProperty MaxStamina;
		public FloatObservableProperty StaminaRegenaration;
		public List<string> Weapon;
		public FloatObservableProperty RollDuration;
		public FloatObservableProperty RollSpeed;
		public FloatObservableProperty RollCost;
        public FloatObservableProperty AttackCost;
        public BoolObservableProperty FuryMode;
		public FloatObservableProperty SpeedWhenReloadCoeff;
	}
}