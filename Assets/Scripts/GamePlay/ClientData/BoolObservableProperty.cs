﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game
{
    [Serializable]
    public class BoolObservableProperty
    {
        [SerializeField]
        private bool value = default;
        public Action<bool, bool> OnChanged = null;
        public bool Value
        {
            get
            {
                return value;
            }
            set
            {
                if (!this.value.Equals(value))
                {
                    var oldValue = this.value;
                    var newValue = value;
                    this.value = value;
                    OnChanged?.Invoke(oldValue, newValue);
                }
            }
        }

        public static implicit operator bool(BoolObservableProperty i) => i.Value;
    }
}
