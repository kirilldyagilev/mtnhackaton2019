﻿using System;

using UnityEngine;

namespace Game
{
	[Serializable]
	public class MobClientData
	{
		public IntObservableProperty Health;
		public IntObservableProperty MaxHealth;
		public IntObservableProperty Speed;
		public IntObservableProperty Strength;
		public FloatObservableProperty AttackDelay;
		public FloatObservableProperty Distance;
		public FloatObservableProperty AttackSpeed;
        public FloatObservableProperty VisionDistance;
		public Sprite Avatar;
		public string Key;
	}
}