﻿using System;
using System.Collections;

using Game;
using Game.Archer;

using UnityEngine;
using UnityEngine.AI;

using AttackState = Game.Archer.AttackState;

namespace GamePlay.Archer2.State
{
	public class Archer2State : DieableState
	{
		private BaseMobController _archerController = null;
		public float _shootSpeed = 0f;
		public float _delayBetweenShoots = 0f;
		private IAttacker _attacker = null;
		[SerializeField] private AttackState _attackState = null;
		private NavMeshAgent _navMeshAgent;
		private Vector3 previousTargetPosition = Vector3.zero;
		public Animator Animator = null;
		public Notifier Notifier = null;

		private void Awake()
		{
			_attacker = GetComponent<IAttacker>();
			_archerController = GetComponent<BaseMobController>();
			_navMeshAgent = GetComponent<NavMeshAgent>();
		}

		public override void Attack(Vector3 attackDirection)
		{
			StartCoroutine(Attack2Coroutine());
		}
		
		private IEnumerator Attack2Coroutine()
		{
			_archerController.ChangeState(_attackState);

			var dir = (PlayerController.Instance.transform.position - transform.position);
			dir.y = 0;
			_attacker.StartAttack(dir.normalized, LayerMask.NameToLayer("Player"));
			_attacker.SetAttackSpeed(_shootSpeed);
			Animator.SetTrigger("Shoot");

			if (Notifier != null)
			{
				Notifier.Notify(1);
			}

			yield return new WaitForSeconds(_delayBetweenShoots);
			if (_archerController.Data.Health <= 0)
			{
				yield break;
			}
			
			dir = (PlayerController.Instance.transform.position - transform.position);
			dir.y = 0;
			_attacker.StartAttack(dir.normalized, LayerMask.NameToLayer("Player"));
			Animator.SetTrigger("Shoot");
			
			yield return new WaitForSeconds(_archerController.Data.AttackDelay);
			if (_archerController.Data.Health <= 0)
			{
				yield break;
			}
			
			_archerController.CheckState();
		}
		
		public override void MoveToTarget(Vector3 target)
		{
			if (Vector3.SqrMagnitude(previousTargetPosition - target) > 0.1f)
			{
				_navMeshAgent.stoppingDistance = 1f;
				_navMeshAgent.speed = _archerController.Data.Speed;
				_navMeshAgent.SetDestination(target);
				previousTargetPosition = target;
				Animator.SetBool("Run", true);
			}
		}
	}
}