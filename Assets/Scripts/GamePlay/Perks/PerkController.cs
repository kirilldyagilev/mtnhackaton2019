﻿using System;
using System.Collections.Generic;

namespace Game
{
    public class PerkController: Handler<PerkActivatedMessage>
    {
        public List<string> activated;
        public event Action<string, bool> OnPerksChanged;
        public event Action OnPerksLoaded;

        protected override void Awake()
        {
            base.Awake();
            activated = new List<string>();
            ProfileOchestrator.CurrentProfileData.Items.ForEach(perk => activated.Add(perk));
			ProfileOchestrator.Items.ForEach(perk => activated.Add(perk));

            OnPerksLoaded?.Invoke();

        }

        public bool IsEnabled(params string[] names)
        {
            foreach (var n in names)
            {
                if (activated.Contains(n))
                    return true;
            }
            return false;
        }

        public override void HandleMessage(PerkActivatedMessage message)
        {
            if (message.enabled)
            {
                activated.Add(message.perk);
            } else
            {
                activated.RemoveAll(x => x == message.perk);
            }

            OnPerksChanged?.Invoke(message.perk, message.enabled);
        }
    }
}
