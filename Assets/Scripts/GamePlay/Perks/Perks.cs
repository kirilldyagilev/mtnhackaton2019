﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public static class Perks
    {
        public const string AttackBoost1 = "item_attackboost_1";
        public const string AttackBoost2 = "item_attackboost_2";
        public const string AttackBoost3 = "item_attackboost_3";
        public const string AttackSpeedBoost1 = "item_attackspeedboost_1";
        public const string AttackSpeedBoost2 = "item_attackspeedboost_2";
        public const string CritMaster1 = "item_crit_master_1";
        public const string CritMaster2 = "item_crit_master_2";
        public const string CritMaster3 = "item_crit_master_3";
        public const string MaxHPIncrease = "item_maxhpincrease";
        public const string Heal = "item_heal";
        public const string BloodThirst = "item_bloodthirst";
        public const string Fury = "item_fury";
        public const string PiercingShot = "item_piercingshot";
        public const string RicochetShot = "item_ricochet";
        public const string RazrivShot = "item_explosivecartridge";
        public const string MultiShot = "item_multishot";
        public const string Vampirism = "item_vampirism";
    }
}
