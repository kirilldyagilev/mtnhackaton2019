﻿using System;
using UnityEngine;

namespace Game
{
	public class BaseMobController : MonoBehaviour, IController, IDamageController
	{
		[SerializeField] protected EnemyData _data = null;
		[SerializeField] protected State _currentState = null;
		[SerializeField] protected State _deathState = null;
		[SerializeField] protected State _idleState = null;
		public Animator Animator = null; 
		
		public MobClientData Data = null;

        public EnemyData BaseData => _data;

        private Vector3 destination = new Vector3(0, 0, 10);

        public event Action<int> OnHPChanged;

		private float time = 1f;
		
		protected virtual void Awake()
		{
			Data.Health.Value = _data.Health;
			Data.Speed.Value = _data.Speed;
			Data.Strength.Value = _data.Strength;
			Data.AttackDelay.Value = _data.AttackDelay;
			Data.Distance.Value = _data.Distance;
			Data.AttackSpeed.Value = _data.AttackSpeed;
            Data.VisionDistance.Value = _data.VisionDistance;
		}

		private void Start()
		{
			EventBus<MobSpawnMessage>.Pub(new MobSpawnMessage() { Mob = this });
		}

		protected virtual void Update()
		{
			if (time > 0)
			{
				time -= Time.deltaTime;
				return;
			}
            if (Vector3.Distance(PlayerController.Instance.transform.position, transform.position) < Data.VisionDistance)
            {

                if (Vector3.Distance(PlayerController.Instance.transform.position, transform.position) < Data.Distance)
                {
                    _currentState.Attack(transform.forward);
                }
                else
                {
                    _currentState.MoveToTarget(PlayerController.Instance.transform.position);                    
                }
				
				if (_currentState != _deathState)
				{
					var dir = (PlayerController.Instance.transform.position - transform.position);
					dir.y = 0;
					Quaternion targetRotation = Quaternion.LookRotation(dir.normalized, Vector3.up);
					transform.rotation = targetRotation;
					/*transform.LookAt(PlayerController.Instance.transform);*/ 
				}
            }
            else
            {
                Wander();
            }
		}

        private void Wander()
        {
            if(Vector3.Distance(destination, transform.position) < 5)
            {
                destination = new Vector3(UnityEngine.Random.Range(-10, 10), transform.position.y, UnityEngine.Random.Range(10, 30f));
            }
            _currentState.MoveToTarget(destination);
        }

		public virtual void ChangeState(State newState)
		{
			_currentState.StateExit(newState);
			_currentState = newState;
			_currentState.StateEnter();
		}

		public virtual void CheckState()
		{
			ChangeState( Data.Health <= 0f ? _deathState : _idleState);
        }

		public State GetState()
		{
			return _currentState;
		}

		public virtual void SetDamage(int damage)
		{
			Data.Health.Value -= damage;
			ChangeState( Data.Health <= 0f ? _deathState : _currentState);

            OnHPChanged?.Invoke(-damage);
        }

		public int GetStrength()
		{
			return Data.Strength;
		}

		public void AfterAttackState()
		{
			
		}

		public void TryDamage(int damageValue)
		{
			_currentState.Damage(damageValue);
		}
	}
}