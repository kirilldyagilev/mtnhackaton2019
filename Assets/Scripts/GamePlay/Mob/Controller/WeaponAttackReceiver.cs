﻿using System.Collections;
using System.Collections.Generic;

using Game;

using UnityEngine;

public class WeaponAttackReceiver : MonoBehaviour
{
    public Attacker Attacker;
    public IController Controller;
    
    // Start is called before the first frame update
    public void WeaponAttack()
    {
        Attacker.WeaponAttack();
    }

    public void WeaponEndAttack()
    {
        Attacker.WeaponEndAttack();
        Attacker.GetComponent<IController>()?.AfterAttackState();
    }
}
