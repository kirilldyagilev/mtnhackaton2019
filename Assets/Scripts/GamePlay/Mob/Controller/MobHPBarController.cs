﻿using System;
using UnityEngine;

namespace Game
{
    public class MobHPBarController : MonoBehaviour
    {
        [SerializeField] private HPBarSprite hpBar;
        private BaseMobController mobController;

        private float showHPBarTimer = 0f;
        private bool isShowing = false;

        private void Awake()
        {
            mobController = GetComponent<BaseMobController>();
            mobController.OnHPChanged += OnMyMobGotSpanked;
            UpdateHealth(1.0f);
            Show(false);
        }

        private void OnDisable()
        {
            mobController.OnHPChanged -= OnMyMobGotSpanked;
        }

        private void OnMyMobGotSpanked(int delta)
        {
            UpdateHealth(Math.Max(0f, mobController.Data.Health.Value) / (mobController.BaseData.Health * 1.0f));
        }

        private void UpdateHealth(float healthNormalized)
        {
            Show(true);
            hpBar.gameObject.SetActive(true);
            hpBar.SetSize(healthNormalized);
            hpBar.SetColor(Color.Lerp(Color.red, Color.green, healthNormalized));
            if (mobController.Data.Health.Value <= 0f)
            {
                Show(false);
            }

        }

        private void Show(bool orly)
        {
            hpBar.gameObject.SetActive(orly);
            isShowing = orly;
            showHPBarTimer = 3f;
        }

        private void Update()
        {
            if (isShowing)
            {
                showHPBarTimer -= Time.deltaTime;
                if (showHPBarTimer <= 0f)
                {
                    Show(false);
                }
            }

        }


    }
}
