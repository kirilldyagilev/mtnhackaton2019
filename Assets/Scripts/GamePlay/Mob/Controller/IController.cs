﻿namespace Game
{
	public interface IController
	{
		void ChangeState(State newState);
		void CheckState();
		State GetState();
		void SetDamage(int damage);
		int GetStrength();
		void AfterAttackState();
	}
}