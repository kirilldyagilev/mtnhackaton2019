﻿using UnityEngine;
using UnityEngine.AI;

namespace Game
{
	public class DeathState : State
	{
        private NavMeshAgent _navMeshAgent;
		public Animator Animator = null;

        private void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        public override void StateEnter()
        {
            _navMeshAgent.enabled = false;
            if (_navMeshAgent.isActiveAndEnabled)
                _navMeshAgent.SetDestination(transform.position);
			EventBus<MobDeathMessage>.Pub(new MobDeathMessage() { Mob = GetComponent<IController>() });
			EventBus<EnemyDeathMessage>.Pub(new EnemyDeathMessage
			{
				deadMob = GetComponent<BaseMobController>()
			});
			
			Animator.SetFloat("SpeedMultiply", 1.0f);
			gameObject.layer = LayerMask.NameToLayer("Default");
			Animator?.SetTrigger("Death");
		}

        public override void MoveToTarget(Vector3 target)
        {
            if (_navMeshAgent.isActiveAndEnabled)
                _navMeshAgent.SetDestination(transform.position);
        }
    }
}