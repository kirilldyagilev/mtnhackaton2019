﻿using UnityEngine;

namespace Game
{
	public class State : MonoBehaviour
	{
		public virtual void Attack(Vector3 direction)
		{
		}

		public virtual void Move(Vector3 directon, float speed)
		{
		}

        public virtual void MoveToTarget(Vector3 target)
        {

        }

        public virtual void Roll(Vector3 direction)
		{
		}

		public virtual void Damage(int damage)
		{
		}

		public virtual void StateEnter()
		{
		}

		public virtual void StateExit(State newState)
		{
		}
	}
}