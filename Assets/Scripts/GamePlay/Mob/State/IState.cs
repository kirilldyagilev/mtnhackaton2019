﻿using UnityEngine;

namespace Game
{
	public interface IState
	{
		void Attack();
		void Move(Vector3 direction);
		void Roll(Vector3 direction);
	}
}