﻿using System.Collections;
using System.Collections.Generic;

using Game;
using Game.Archer;

using UnityEngine;

public class AfterAttackState : DieableState
{
	private Rigidbody _rigidBody = null;
	public Animator Animator;
	private void Awake()
	{
		_rigidBody = GetComponent<Rigidbody>();
	}
	public State RollState = null;
	
	public override void Roll(Vector3 direction)
	{
		if (PlayerController.Instance.Data.Stamina < PlayerController.Instance.Data.RollCost)
		{
			return;
		}

		transform.forward = direction;

		PlayerController.Instance.ChangeState(RollState);
		PlayerController.Instance.Data.Stamina.Value -= PlayerController.Instance.Data.RollCost;
		PlayerController.Instance.Roll(direction);
	}
	
	public override void Move(Vector3 direction, float speed)
	{
		if (Mathf.Approximately(direction.sqrMagnitude, 0))
		{
			Animator.SetBool("IsRun", false);
			return;
		}
		Animator.SetBool("IsRun", true);
		var newPosition = transform.position + PlayerController.Instance.Data.Speed * PlayerController.Instance.Data.SpeedWhenReloadCoeff * Time.deltaTime * direction;
		_rigidBody.MovePosition(newPosition);

		var newRotation = Quaternion.LookRotation(direction, Vector3.up);
		_rigidBody.MoveRotation(newRotation);
			
		print("Attack move");
	}
}
