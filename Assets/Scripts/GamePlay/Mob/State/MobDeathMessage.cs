﻿namespace Game
{
	public class MobDeathMessage : Message
	{
		public IController Mob;
	}

	public class MobSpawnMessage : Message
	{
		public IController Mob;
	}

	public class WinMessage : Message
	{
		public string Perk;
		public int Health;
		public int Attack;
		public int Stamina;
	}
}