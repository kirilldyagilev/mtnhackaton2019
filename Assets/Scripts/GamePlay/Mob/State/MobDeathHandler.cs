﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class MobDeathHandler : Handler<MobDeathMessage>
	{
		private List<IController> mobs = new List<IController>();
		public GameObject EnableOnAllDeath;
		public AudioClip WinJingle;

		private bool played = false;
		protected override void Awake()
		{
			base.Awake();
			EventBus<MobSpawnMessage>.Sub(HandleMessage);
		}
		protected override void OnDestroy()
		{
			base.OnDestroy();
			EventBus<MobSpawnMessage>.Unsub(HandleMessage);
		}
		public override void HandleMessage(MobDeathMessage message)
		{
			mobs.Remove(message.Mob);
			if (mobs.Count == 0 && !played)
			{
				played = true;
				EnableOnAllDeath.SetActive(true);
				SoundSystem.Fade(0.75f,4.25f);
				SoundSystem.PlaySound(WinJingle);
				PlayerController.Instance.Godlike = true;
			}
		}
		public void HandleMessage(MobSpawnMessage message)
		{
			mobs.Add(message.Mob);
		}
	}
}