﻿using UnityEngine;

namespace Game
{
	public class Portal : MonoBehaviour
	{
		public Dialogue Dialogue;
		private bool opened = false;
		private void OnTriggerEnter(Collider other)
		{
			if (opened)
			{
				return;
			}
			if (other.GetComponent <PlayerController>() == null)
			{
				return;
			}
			Dialogue.OpenNow();
			opened = true;
		}
	}
}