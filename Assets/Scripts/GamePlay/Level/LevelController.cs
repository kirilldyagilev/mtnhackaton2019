﻿using UnityEngine;

namespace Game
{
	public class LevelController : MonoBehaviour
	{
		public void LoadScene(string loadScene)
		{
			SceneLoader.Instance.LoadScene(loadScene);
		}
	}
}

