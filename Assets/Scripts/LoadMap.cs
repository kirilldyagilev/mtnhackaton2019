﻿using Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadMap : StateMachineBehaviour
{
	public bool NextEncounter;
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (NextEncounter)
		{
			EventBus<NextEncounterMessage>.Pub(new NextEncounterMessage());
		}
		else
		{
			EventBus<ReturnToMapMessage>.Pub(new ReturnToMapMessage());
		}
	}
}
