﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bob : MonoBehaviour
{
    [SerializeField] private float _speed = 1f;
    [SerializeField] private float _maxOffset = 0.35f;
    private float _yPosition;
    
    private void Start()
    {
        _yPosition = transform.position.y;
    }

    private void Update()
    {
        var alpha = Mathf.Sin(Time.time * _speed);
        var position = transform.position;
        var y = _yPosition + _maxOffset * alpha;
        position.y = y;
        transform.position = position;
    }
}
