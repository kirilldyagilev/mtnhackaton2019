﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadCharacterAnimator : MonoBehaviour
{
    private Animator _animator;
    [SerializeField] private string[] _triggers;
    [SerializeField] private float _delayMin = 3f;
    [SerializeField] private float _delayMax = 6f;
    
    private void Start()
    {
        _animator = GetComponent<Animator>();
        StartCoroutine(PlayRandomAnimation());
    }

    private IEnumerator PlayRandomAnimation()
    {
        yield return new WaitForSeconds(Random.Range(_delayMin, _delayMax));
        var random = Random.Range(0, _triggers.Length);
        var target = _triggers[random];
        _animator.SetTrigger(target);
        yield return PlayRandomAnimation();
    }
}
