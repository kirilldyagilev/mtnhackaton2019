﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Map.View
{
    public class MoveWithObject : MonoBehaviour
    {
        [SerializeField] private Transform _target;
        [SerializeField] private float _distance = 18f;
        [SerializeField] private float _moveDistance = 54f;
        [SerializeField] private Vector3 _moveDirection = Vector3.forward;
        private void Start()
        {
            InvokeRepeating(nameof(CheckDistance), 0.5f, 0.5f);
        }

        private void CheckDistance()
        {
            if(_target.position.z > transform.position.z)
            {
                if(Vector3.Distance(transform.position, _target.position) > _distance)
                {
                    transform.position = transform.position + _moveDirection * _moveDistance;
                }
            }
        }
    }
}
