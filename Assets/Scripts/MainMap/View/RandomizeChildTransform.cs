﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizeChildTransform : MonoBehaviour
{
    [SerializeField] private float _min = 0.9f;
    [SerializeField] private float _max = 1.1f;

    [ContextMenu("RandomizeScale")]
    private void RandomizeScale()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            var scale = child.localScale;
            scale *= Random.Range(_min, _max);
            child.localScale = scale;
        }
    }

    [ContextMenu("RandomizeRotation")]
    private void RandomizeRotation()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            var euler = child.rotation.eulerAngles;
            child.rotation = Quaternion.Euler(new Vector3(euler.x, Random.Range(0, 360), euler.z));
        }
    }
}
