﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    [SerializeField] private Vector3 _axis = Vector3.right;
    [SerializeField] private float _speed = 720f;
    private void Start()
    {
        
    }

    private void Update()
    {
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + _axis * _speed * Time.deltaTime);
    }
}
