﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowPolyGroundMeshGenerator : MonoBehaviour
{
    private MeshRenderer _meshRenderer;
    private MeshFilter _meshFilter;

    [SerializeField] private int _sizeX = 100;
    [SerializeField] private int _sizeZ = 100;
    [SerializeField] private float _range = 100f;
    [SerializeField] private float _width = 10f;
    [SerializeField] private float _step = 0.1f;
    [SerializeField] private float _maxHeight = 0.1f;
    [SerializeField] private float _noiseMultiplier = 0.3f;
    private List<Vector3> _vertices = new List<Vector3>();
    private List<int> _tris = new List<int>();
        
    
    private void Start()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _meshFilter = GetComponent<MeshFilter>();
        Generate();
    }

    [ContextMenu("Generate")]
    private void Generate()
    {
        _vertices.Clear();
        int i = 0;
        for(int x = 0; x <= _sizeX; x++)
        {
            for(int z = 0; z <= _sizeZ; z++)
            {
                var noise = Mathf.PerlinNoise(x * _noiseMultiplier, z * _noiseMultiplier);
                var vertex = new Vector3(x * _step, noise * _maxHeight, z * _step);
                _vertices.Add(vertex);
                i++;
            }
        }
        var triangles = new int[_sizeX * _sizeZ * 6];

        int vert = 0;
        int tris = 0;
        for(int x = 0; x < _sizeX; x++)
        {
            for(int z = 0; z < _sizeZ; z++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + _sizeX + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + _sizeX + 1;
                triangles[tris + 5] = vert + _sizeX + 2;
                vert++;
                tris += 6;
            }
            vert++;
        }
        var mesh = new Mesh();
        mesh.SetVertices(_vertices);
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
        _meshFilter.mesh = mesh;
    }
}
