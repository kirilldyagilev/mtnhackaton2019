﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Map.View
{
    public class Move : MonoBehaviour
    {
        [SerializeField] private Vector3 _direction = Vector3.forward;
        [SerializeField] private float _speed = 20f;

        private void Start()
        {

        }

        private void Update()
        {
            var position = transform.position;
            position += _direction * _speed * Time.deltaTime;
            transform.position = position;
        }
    }
}