﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayParticleOnEvent : MonoBehaviour
{
    [SerializeField] private ParticleSystem _particleSystem;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Steam()
    {
        _particleSystem.Play();
    }
}
