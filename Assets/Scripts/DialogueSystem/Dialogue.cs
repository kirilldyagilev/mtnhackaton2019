﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/DialogueSystem/Dialogue", fileName = "Dialogue", order = 0)]
public class Dialogue : ScriptableObject
{
	public string Key;
	public List<DialogueOption> DialogueOptions;

	[ContextMenu("OpenNow")]
	public void OpenNow()
	{
		EventBus<OpenDialogMessage>.Pub(new OpenDialogMessage() { Dialogue = this });
	}
}
