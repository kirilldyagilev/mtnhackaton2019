﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{

	public class DialogueOptionView : MonoBehaviour
	{
		[SerializeField] private DialogueOption option;
		public TMPro.TMP_Text Text;
		public Image Image;
		public Func<bool> GetPicked = null;
		public Action<bool> SetPicked = null;
		public void SetOption(DialogueOption option)
		{
			if (option)
			{
				if(option is TakePerk perk)
				{
					var newPerk = ScriptableObject.CreateInstance<TakePerk>();
					newPerk.Key = perk.Key;
					newPerk.Perks = perk.Perks;
					option = newPerk;
					Image.sprite = newPerk.GetSprite();
					Text.text = LocalizationSystem.Text["ui_"+newPerk.GetPerk()];
				}
				else
				{
					Text.text = LocalizationSystem.Text[option.Key];
					Image.sprite = option.Sprite;
				}
				
			}
			this.option = option;
		}

		public void Show()
		{
			gameObject.SetActive(true);
		}

		public void Hide()
		{
			gameObject.SetActive(false);
		}

		public void OnClick()
		{
			if (GetPicked())
			{
				return;
			}
			SetPicked(true);
			if (option) option.OnPick();
		}
	}
}