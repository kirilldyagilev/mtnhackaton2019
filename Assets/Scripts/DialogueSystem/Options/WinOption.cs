﻿using Game;

[UnityEngine.CreateAssetMenu(menuName = "Game/DialogueSystem/WinOption", fileName = "WinOption")]
public class WinOption : DialogueOption
{
	public override void OnPick()
	{
		EventBus<WinMessage>.Pub(new WinMessage());
	}
}
