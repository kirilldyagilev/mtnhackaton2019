﻿using Game;
using System;
using System.Collections.Generic;
using UnityEngine;

[UnityEngine.CreateAssetMenu(menuName = "Game/DialogueSystem/TakePerk", fileName = "TakePerk")]
public class TakePerk : DialogueOption
{
	[System.Serializable]
	public class ItemProbability
	{
		public string Item;
		public float Weight;
		public Sprite Sprite;
	}
	public List<ItemProbability> Perks;

	private string _perk = null;

	public override void OnPick()
	{
		var perk = GetPerk();
		var pc = PlayerController.Instance;
		if(perk == Game.Perks.Heal)
		{
            var amount = Convert.ToInt32(Mathf.Ceil(PlayerController.Instance.Data.Health * 0.2f));
            perk = null;
            Game.ProfileOchestrator.CurrentHP += amount;
            EventBus<PlayerHealMessage>.Pub(new PlayerHealMessage()
            {
                healValue = amount
            });
		}
		EventBus<WinMessage>.Pub(new WinMessage() { Perk = perk, Health = pc.Health, Attack = pc.Attack, Stamina = pc.Stamina});
	}

	public Sprite GetSprite()
	{
		var chosen = GetPerk();
		foreach (var perk in Perks)
		{
			if(chosen == perk.Item)
			{
				return perk.Sprite;
			}
		}
		return null;
	}

	public string GetPerk()
	{
		if(_perk == null)
		{
			_perk = GetRandomPerk();
		}
		return _perk;
	}

	private string GetRandomPerk()
	{
		if (Perks.Count == 0)
		{
			return null;
		}
		float s = 0;
		foreach (var perk in Perks)
		{
			s += perk.Weight;
		}
		float r = UnityEngine.Random.value * s;
		s = 0;
		foreach (var perk in Perks)
		{
			s += perk.Weight;
			if (r < s)
			{
				return perk.Item;
			}
		}
		return Perks[Perks.Count - 1].Item;
	}
}
