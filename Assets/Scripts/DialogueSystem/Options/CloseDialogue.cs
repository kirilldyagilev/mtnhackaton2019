﻿[UnityEngine.CreateAssetMenu(menuName = "Game/DialogueSystem/CloseDialogue", fileName = "CloseDialogue")]
public class CloseDialogue : DialogueOption
{
	public override void OnPick()
	{
		EventBus<CloseDialogMessage>.Pub(new CloseDialogMessage() );
	}
}