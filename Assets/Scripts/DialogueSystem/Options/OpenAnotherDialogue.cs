﻿[UnityEngine.CreateAssetMenu(menuName = "Game/DialogueSystem/OpenDialogue", fileName = "OpenDialogue")]
public class OpenAnotherDialogue : DialogueOption
{
	public Dialogue Dialogue;
	public override void OnPick()
	{
		EventBus<OpenDialogMessage>.Pub(new OpenDialogMessage() { Dialogue = Dialogue });
	}
}
