﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{

	public class DialogueSystem : MonoBehaviour
	{
		public static DialogueSystem Instance;
		public GameObject DialogWindow;

		public TMPro.TMP_Text Text;

		public Dialogue CurrentDialogue;
		public List<DialogueOptionView> DialogueOptionViews;
		public Dungeon.DungeonMap Map;
		public TMPro.TMP_Text Progress;
		private bool picked = false;
		private void Awake()
		{
			Instance = this;
			DialogWindow.SetActive(false);
			foreach (var view in DialogueOptionViews)
			{
				view.SetPicked = (v) => picked = v;
				view.GetPicked = () => picked;
			}
		}

		public void Open(Dialogue newDialog)
		{
			Text.text = LocalizationSystem.Text[newDialog.Key];
			DialogWindow.SetActive(true);
			CurrentDialogue = newDialog;
			InitializeOptions(newDialog.DialogueOptions);
			Progress.text = LocalizationSystem.Text["ui_txt_yougained"] + "\n";
			var encounter = Map.Seasons[ProfileOchestrator.CurrentProfileData.ActiveSeason].Encounters[ProfileOchestrator.CurrentEncounter];
			if (encounter.HP > 0)
			{
				Progress.text += "+" + encounter.HP + " " + LocalizationSystem.Text["ui_txt_hp"] + "\n";
			}
			if(encounter.ATK > 0)
			{
				Progress.text += "+" + encounter.ATK + " " + LocalizationSystem.Text["ui_txt_atk"] + "\n";
			}
			if (encounter.STM > 0)
			{
				Progress.text += "+" + encounter.STM + " " + LocalizationSystem.Text["ui_txt_stamina"] + "\n";
			}
		}

		public void Close()
		{
			DialogWindow.SetActive(false);
		}

		public void InitializeOptions(List<DialogueOption> options)
		{
			for (int i = 0, c = DialogueOptionViews.Count; i < c; i++)
			{
				if (i < options.Count)
				{
					DialogueOptionViews[i].Show();
					DialogueOptionViews[i].SetOption(options[i]);
				}
				else
				{
					DialogueOptionViews[i].Hide();
					DialogueOptionViews[i].SetOption(null);
				}
			}
		}
	}
}