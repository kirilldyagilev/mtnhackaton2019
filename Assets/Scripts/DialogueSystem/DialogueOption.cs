﻿using UnityEngine;

public abstract class DialogueOption : ScriptableObject
{
	public string Key;
	public Sprite Sprite;
	public abstract void OnPick();
}