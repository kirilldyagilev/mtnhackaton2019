﻿using UnityEngine;

namespace Game.Titles
{
	public class ReturnToMap : MonoBehaviour
	{
		public void Return()
		{
			EventBus<ReturnToMapMessage>.Pub(new ReturnToMapMessage());
		}
	}

}
