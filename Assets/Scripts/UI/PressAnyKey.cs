﻿using UnityEngine;
using UnityEngine.Events;

public class PressAnyKey:MonoBehaviour
{
	public UnityEvent OnPress;

	private void Update()
	{
		if (Input.anyKeyDown)
		{
			enabled = false;
			OnPress?.Invoke();
		}
	}
}