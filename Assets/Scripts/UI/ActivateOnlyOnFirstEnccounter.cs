﻿using System;
using UnityEngine;

namespace Game
{
	public class ActivateOnlyOnFirstEnccounter :MonoBehaviour
	{
		private void Awake()
		{
			var enc = ProfileOchestrator.CurrentEncounter;
			var season = ProfileOchestrator.CurrentProfileData.ActiveSeason;

			gameObject.SetActive(enc+ season == 0);
		}

		private void OnDisable()
		{
			Time.timeScale = 1;
		}
	}
}
