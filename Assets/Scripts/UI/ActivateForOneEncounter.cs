﻿using UnityEngine;

namespace Game
{
	public class ActivateForOneEncounter : MonoBehaviour
	{
		public int Encounter = 10;
		private void Awake()
		{
			var enc = ProfileOchestrator.CurrentEncounter;

			gameObject.SetActive(enc  == Encounter);
		}
	}
}
