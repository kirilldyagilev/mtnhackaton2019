﻿using UnityEngine;

namespace Assets.Scripts.UI
{
	[RequireComponent(typeof(Animator))]
	public class AnimatorTrigger:MonoBehaviour
	{
		private Animator animator = null;
		public string Trigger;
		private void Awake()
		{
			animator = GetComponent<Animator>();
		}

		public void PushTrigger()
		{
			animator.SetTrigger(Trigger);
		}
	}
}
