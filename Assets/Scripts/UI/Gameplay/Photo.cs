﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
	public class Photo : MonoBehaviour
	{
		public Image PhotoImage = null;
		public TMPro.TMP_Text Text;
		public void Set(Sprite sprite, string key)
		{
			PhotoImage.sprite = sprite;
			Text.text = LocalizationSystem.Text[key];
		}
	}
}
