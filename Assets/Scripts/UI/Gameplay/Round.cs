﻿using DG.Tweening;
using Game.Dungeon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game
{
	public class Round : MonoBehaviour
	{
		public Dungeon.DungeonMap Map;
		public Photo prefab;
		public Transform parent;

		public Photo MainHero;
		public Sprite Avatar;
		public string Key;
		public TMPro.TMP_Text RoundText;

		public CanvasGroup CanvasGroup;
		private bool closed = false;

		private void Awake()
		{
			DOTween.defaultTimeScaleIndependent = true;
			MainHero.Set(Avatar, Key);
			var encounter = Map.Seasons[ProfileOchestrator.CurrentProfileData.ActiveSeason].Encounters[ProfileOchestrator.CurrentEncounter];
			foreach (var mob in (encounter as BattleEncounter).Mobs)
			{
				var photo = Instantiate(prefab, parent);
				photo.Set(mob.Data.Avatar, mob.Data.Key);
			}
			RoundText.text = LocalizationSystem.Text["ROUND"] + " " + (ProfileOchestrator.CurrentEncounter + 1).ToString();
			CanvasGroup.alpha = 0;
			CanvasGroup.DOFade(1, .5f).SetUpdate(true);
			CanvasGroup.gameObject.SetActive(true);
			Time.timeScale = 0;
		}

		public void Close()
		{
			if (closed)
			{
				return;
			}
			closed = true;
			print("CLOSE");
			CanvasGroup.interactable = false;
			CanvasGroup.blocksRaycasts = false;
			CanvasGroup.DOFade(1, .5f).SetUpdate(true).OnComplete(()=> 
			{
				gameObject.SetActive(false);
				var tutorial = FindObjectOfType<ActivateOnlyOnFirstEnccounter>();
				if(!tutorial)
				{
					Time.timeScale = 1;
				}
				else if(!tutorial.isActiveAndEnabled)
				{
					Time.timeScale = 1;
				}
				var encounter = Map.Seasons[ProfileOchestrator.CurrentProfileData.ActiveSeason].Encounters[ProfileOchestrator.CurrentEncounter];
				foreach (var mob in (encounter as BattleEncounter).Mobs)
				{
					var position = (encounter as BattleEncounter).MobsZoneAnchor + new Vector2(Random.value * (encounter as BattleEncounter).MobsZoneSize.x, Random.value * (encounter as BattleEncounter).MobsZoneSize.y);

					Instantiate(mob.gameObject, new Vector3(position.x, 2, position.y), Quaternion.Euler(0, Random.value * 360, 0));
				}
			});
		}
	}
}
