﻿using Game.Dungeon;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{

	public class MapWindow : MonoBehaviour
	{
		public Image Progress;

		public Dungeon.DungeonMap DungeonMap;
		public GameObject MapObject;
		private ProfileData Profile => ProfileOchestrator.CurrentProfileData;

		private void Awake()
		{
			EventBus<OpenMapMessage>.Sub(HandleMessage);
			EventBus<CloseMapMessage>.Sub(HandleMessage);
			EventBus<BattleMapMessage>.Sub(HandleMessage);
			Close();
		}

		private void OnDestroy()
		{
			EventBus<OpenMapMessage>.Unsub(HandleMessage);
			EventBus<CloseMapMessage>.Unsub(HandleMessage);
			EventBus<BattleMapMessage>.Unsub(HandleMessage);
		}

		private void HandleMessage(OpenMapMessage message)
		{
			Open();
		}

		private void HandleMessage(CloseMapMessage message)
		{
			Close();
		}

		private void HandleMessage(BattleMapMessage message)
		{
			Battle();
		}

		public void Battle()
		{
			var currentSeason = Profile.ActiveSeason;
			if (DungeonMap.Seasons.Count <= currentSeason)
			{
				return;
			}
			DungeonMap.Seasons[currentSeason].Encounters[0].Start();
		}

		public void Close()
		{
			MapObject.SetActive(false);
			EventBus<ClosedMapMessage>.Pub(new ClosedMapMessage());
		}

		public void Open()
		{
			MapObject.SetActive(true);
			var currentSeason = Profile.ActiveSeason;
			if (DungeonMap.Seasons.Count <= currentSeason)
			{
				Progress.fillAmount = 1;
				return;
			}
			Progress.fillAmount = DungeonMap.Seasons[currentSeason].Progress;
			EventBus<OpenedMapMessage>.Pub(new OpenedMapMessage());
		}
	}
}