﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Map.View
{
    public enum QuoteAuthor
    {
        MainHero,
        Marina,
        Evgeniy,
        Natasha
    }
    
    public class RoadDialogueSystem : MonoBehaviour
    {
        [SerializeField] private Quote[] _storyQuotes;
        [SerializeField] private float _quoteDuration = 5f;
        [SerializeField] private Quote[] _randomQuotes;
        private int _activeStoryQuoteIndex = -1;
        private QuoteView[] _quotes;
        private bool _storyEnded;
        private float _randomQuoteDelay = 6f;
        [SerializeField] private int _storyIndex = 0;
        public float Duration = 3;

        
        private void Awake()
        {
            _quotes = FindObjectsOfType<QuoteView>();
            _storyEnded = ProfileOchestrator.CurrentProfileData.CurrentStoryIndex > _storyIndex;
        }

        private void Start()
        {
            if(!_storyEnded)
            {
                if(_storyQuotes.Length == 0)
                {
                    _storyEnded = true;
                    StartCoroutine(SayRandomQuote());
                }
                else
                {
                    StartCoroutine(ActivateStoryQuote(0));
                }
            }
            else
            {
                StartCoroutine(SayRandomQuote());
            }
            ProfileOchestrator.CurrentProfileData.CurrentStoryIndex = _storyIndex + 1;
            ProfileOchestrator.Save();
        }

        private IEnumerator ActivateStoryQuote(int index)
        {
            var quote = _storyQuotes[index];
            yield return SayQuote(quote);
            if(_storyQuotes.Length > index + 1)
            {
                StartCoroutine(ActivateStoryQuote(index + 1));
            }
            else
            {
                StartCoroutine(SayRandomQuote());
            }
        }

        private IEnumerator SayQuote(Quote quote)
        {
            var view = GetByAuthor(quote.Author);
            view.Appear(LocalizationSystem.GetText(quote.Text));
            yield return new WaitForSeconds(Duration);
            view.Disappear();
            yield return new WaitForSeconds(view.AppearDuration);
        }

        private IEnumerator SayRandomQuote()
        {
            yield return new WaitForSeconds(_randomQuoteDelay);
            yield return SayQuote(_randomQuotes.Random<Quote>());
            yield return SayRandomQuote();
        }

        private QuoteView GetByAuthor(QuoteAuthor author)
        {
            foreach(var storyQuote in _quotes)
            {
                if(storyQuote.QuoteAuthor == author)
                {
                    return storyQuote;
                }
            }
            return null;
        }
    }

    [Serializable]
    public class Quote
    {
        public QuoteAuthor Author;
        public string Text;
    }
}