﻿using UnityEngine;
using UnityEngine.Events;

namespace Game
{
	public class OpenCloseMapHadler: MonoBehaviour
	{
		public UnityEvent OnOpen;
		public UnityEvent OnClose;

		private void Awake()
		{
			EventBus<OpenedMapMessage>.Sub(HandleMessage);
			EventBus<ClosedMapMessage>.Sub(HandleMessage);
		}
		private void OnDestroy()
		{
			EventBus<OpenedMapMessage>.Unsub(HandleMessage);
			EventBus<ClosedMapMessage>.Unsub(HandleMessage);
		}

		private void HandleMessage(OpenedMapMessage message)
		{
			OnOpen?.Invoke();
		}
		private void HandleMessage(ClosedMapMessage message)
		{
			OnClose?.Invoke();
		}
	}
}