﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Game.Map.View
{
    public class QuoteView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _text;
        private CanvasGroup _canvasGroup;
        [SerializeField] private float _appearDuration = 0.5f;
        public QuoteAuthor QuoteAuthor;

        public float AppearDuration => _appearDuration;

        private void Awake()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        public void Appear(string text)
        {
            _text.text = text;
            StartCoroutine(Appear());
        }

        public void Disappear()
        {
            StartCoroutine(DisappearCoroutine());
        }

        private IEnumerator Appear()
        {
            float elapsedTime = 0f;
            while(elapsedTime < _appearDuration)
            {
                elapsedTime += Time.deltaTime;
                _canvasGroup.alpha = elapsedTime / _appearDuration;
                yield return null;
            }

            _canvasGroup.alpha = 1f;
        }

        private IEnumerator DisappearCoroutine()
        {
            float elapsedTime = 0f;
            while(elapsedTime < _appearDuration)
            {
                elapsedTime += Time.deltaTime;
                _canvasGroup.alpha = 1f - elapsedTime / _appearDuration;
                yield return null;
            }

            _canvasGroup.alpha = 0f;
        }
    }
}
