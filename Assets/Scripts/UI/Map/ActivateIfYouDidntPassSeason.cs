﻿using UnityEngine;

namespace Game
{
	public class ActivateIfYouDidntPassSeason : MonoBehaviour
	{
		public int Season;
		private void Awake()
		{
			gameObject.SetActive(ProfileOchestrator.CurrentProfileData.ActiveSeason < Season);
		}
	}
}