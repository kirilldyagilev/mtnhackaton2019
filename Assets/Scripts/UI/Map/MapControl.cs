﻿using UnityEngine;

namespace Game
{
	public class MapControl : MonoBehaviour
	{
		public void Open()
		{
			EventBus<OpenMapMessage>.Pub(new OpenMapMessage());
		}

		public void Close()
		{
			EventBus<CloseMapMessage>.Pub(new CloseMapMessage());
		}

		public void Battle()
		{
			EventBus<BattleMapMessage>.Pub(new BattleMapMessage());
		}
	}
}