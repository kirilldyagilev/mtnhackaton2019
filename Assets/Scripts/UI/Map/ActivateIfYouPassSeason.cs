﻿using UnityEngine;

namespace Game
{
	public class ActivateIfYouPassSeason : MonoBehaviour
	{
		public int Season;
		private void Awake()
		{
			gameObject.SetActive(ProfileOchestrator.CurrentProfileData.ActiveSeason >= Season);
		}
	}
}