﻿using UnityEngine;
namespace Game
{
	public class Skull : MonoBehaviour
	{
		public TMPro.TMP_Text DeathCountText;
		public int Key = 0;
		public void Awake()
		{
			var count = ProfileOchestrator.CurrentProfileData.Deaths.TryGetValue(Key, out int c) ? c : 0;
			gameObject.SetActive(count > 0);
			DeathCountText.text = $"{count}";
		}
	}
}