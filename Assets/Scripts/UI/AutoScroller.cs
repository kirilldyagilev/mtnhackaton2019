﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Titles
{
	public class AutoScroller : MonoBehaviour
	{
		[SerializeField] private float _speed = 5f;

		void Start()
		{

		}

		private void Update()
		{
			transform.position = transform.position + Vector3.up * _speed * Time.deltaTime;
		}
	}

}
