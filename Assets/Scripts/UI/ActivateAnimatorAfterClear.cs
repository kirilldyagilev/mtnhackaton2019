﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
	public class ActivateAnimatorAfterClear : Handler<MobDeathMessage>
	{
		private List<IController> mobs = new List<IController>();
		public Animator Animator;
		public string Trigger;
		protected override void Awake()
		{
			base.Awake();
			EventBus<MobSpawnMessage>.Sub(HandleMessage);
		}
		protected override void OnDestroy()
		{
			base.OnDestroy();
			EventBus<MobSpawnMessage>.Unsub(HandleMessage);
		}
		public override void HandleMessage(MobDeathMessage message)
		{
			mobs.Remove(message.Mob);
			if (mobs.Count == 0)
			{
				Animator.SetTrigger(Trigger);
			}
		}
		public void HandleMessage(MobSpawnMessage message)
		{
			mobs.Add(message.Mob);
		}
	}
}
