﻿using Game;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DamagePopup : MonoBehaviour
{
    [SerializeField] private TextMeshPro textMesh;
    private Rigidbody rigidBody;
    private float disappearTimer = 3f;

    private void Awake()
    {
        Setup(0, false);
    }

    public void Setup(int damage, bool crit)
    {
        rigidBody = GetComponent<Rigidbody>();
        if (crit)
            textMesh.fontSize = textMesh.fontSize + 2;
        textMesh.text = $"-{damage}";
        if (crit)
            textMesh.outlineColor = Color.red;
        else if (damage < 0)
        {
            textMesh.text = $"+{-damage}";
            textMesh.outlineColor = Color.green;
        }
        else
            textMesh.outlineColor = Color.black;
        rigidBody.velocity = new Vector3(Random.Range(-3f, 3f), Random.Range((5f + (crit?2f:0f)), 8f+ (crit ? 2f : 0f)), Random.Range(-3f, 3f));
    }

    private void Update()
    {
        disappearTimer -= Time.deltaTime;
        if (disappearTimer < 0)
        {
            Destroy(gameObject);
        }
    }
}
