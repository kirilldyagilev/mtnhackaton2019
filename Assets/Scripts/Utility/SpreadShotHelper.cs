﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game
{
    public static class SpreadShotHelper
    {
        public static Vector3[] SpreadDirections(Vector3 direction, int num, int spreadAngle)
        {
            Vector3[] result = new Vector3[num];
            Quaternion _rotation = Quaternion.Euler(0f, direction.y - ((num - 1) * spreadAngle) / 2, 0f);
            Vector3 newDir = _rotation * direction;
            for (int i = 0; i < num; i++)
            {
                result[i] = newDir;
                _rotation = Quaternion.Euler(0f, newDir.y + spreadAngle, 0f);
                newDir = _rotation * newDir;
            }
            return result;
        }

    }
}
