﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    static class FindGameObjectsByLayer
    {
        public static List<GameObject> Find(int layer)
        {
            var result = new List<GameObject>();
            var objects = UnityEngine.Object.FindObjectsOfType(typeof(GameObject));
            foreach (GameObject obj in objects)
            {
                if (obj.layer == layer)
                    result.Add(obj);
            }
            return result;
        }

    }
}
