﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
	public class SceneLoaderView : MonoBehaviour
	{
		[SerializeField] private Image _image;
		[SerializeField] private float duration;

		public void Awake()
		{
			DontDestroyOnLoad(gameObject);
		}

		public void Start()
		{
			SceneLoader.Instance.OnSceneLoadingStarted += Appearing;
			SceneLoader.Instance.OnSceneLoadingCompleted += Fading;
		}

		public IEnumerator Fading(string sceneName)
		{
			yield return _image.DOFade(0, duration).WaitForCompletion();
		}

		public IEnumerator Appearing(string sceneName)
		{
			yield return _image.DOFade(1, duration).WaitForCompletion();
		}
	}
}