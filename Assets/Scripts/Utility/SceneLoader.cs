﻿using System;
using System.Collections;

using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
	public class SceneLoader : MonoBehaviour
	{
		public static SceneLoader Instance { get; private set; }

		public Action<string, float> OnSceneLoadingProgress { get; set; }
		public Func<string, IEnumerator> OnSceneLoadingStarted { get; set; }
		public Func<string, IEnumerator> OnSceneLoadingCompleted { get; set; }
		public Action OnStartLoading { get; set; }
		public Action OnEndLoading { get; set; }
		public Dungeon.DungeonMap DungeonMap;
		private static string[] MapScenes = new string[] { "MapScene_Winter", "MapScene_Spring", "MapScene_Summer", "MapScene_Autumn", "MapScene_Winter" };

		public string MapSceneName=>MapScenes[ProfileOchestrator.CurrentProfileData.ActiveSeason];

		private void Awake()
		{
			if (Instance != null)
			{
				DestroyImmediate(gameObject);
				return;
			}
			Instance = this;
			DontDestroyOnLoad(gameObject);
			EventBus<NextEncounterMessage>.Sub(HandleLoadEncounterMessage);
			EventBus<ReturnToMapMessage>.Sub(HandleReturnToMapMessagee);
			EventBus<LoadSceneMessage>.Sub(HandleLoadSceneMessagee);
		}


		private void OnDestroy()
		{
			if (Instance == this) Instance = null;
			EventBus<NextEncounterMessage>.Unsub(HandleLoadEncounterMessage);
			EventBus<ReturnToMapMessage>.Unsub(HandleReturnToMapMessagee);
			EventBus<LoadSceneMessage>.Unsub(HandleLoadSceneMessagee);
		}

		private void HandleLoadEncounterMessage(NextEncounterMessage message)
		{
			var currentSeason = ProfileOchestrator.CurrentProfileData.ActiveSeason;
			if (DungeonMap.Seasons.Count <= currentSeason)
			{
				return;
			}
			var encounter = ProfileOchestrator.CurrentEncounter >= DungeonMap.Seasons[currentSeason].Encounters.Count ? DungeonMap.Seasons[currentSeason].Encounters.Count -1: ProfileOchestrator.CurrentEncounter;
			DungeonMap.Seasons[currentSeason].Encounters[encounter].Start();
		}

		private void HandleReturnToMapMessagee(ReturnToMapMessage message)
		{
			LoadScene(MapSceneName);
		}

		private void HandleLoadSceneMessagee(LoadSceneMessage message)
		{
			LoadScene(message.Scene);
		}

		public void LoadScene(string sceneName)
		{
			StartCoroutine(LoadingScene(sceneName));
		}

		private IEnumerator LoadingScene(string sceneName)
		{
			yield return OnSceneLoadingStarted?.Invoke(sceneName);

			OnStartLoading?.Invoke();

			var asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
			while (!asyncLoad.isDone)
			{
				OnSceneLoadingProgress?.Invoke(sceneName, asyncLoad.progress);
				yield return new WaitForEndOfFrame();
			}
			OnEndLoading?.Invoke();
			yield return OnSceneLoadingCompleted?.Invoke(sceneName);
		}
	}
}