﻿namespace Game
{
	public class ReturnToMapMessage : Message
	{

	}

	public class LoadSceneMessage : Message
	{
		public string Scene;
		public LoadSceneMessage(string scene)
		{
			Scene = scene;
		}
	}
}