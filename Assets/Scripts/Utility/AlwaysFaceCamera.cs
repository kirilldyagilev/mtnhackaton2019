﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game
{
    public class AlwaysFaceCamera : MonoBehaviour
    {
        private Camera mainCamera;

        protected virtual void Awake()
        {
            mainCamera = Camera.main;
        }

        protected virtual void Update()
        {
            transform.LookAt(transform.position + mainCamera.transform.rotation * Vector3.forward, mainCamera.transform.rotation * Vector3.up);
        }

    }
}
