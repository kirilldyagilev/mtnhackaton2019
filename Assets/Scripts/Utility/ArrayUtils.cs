using System;
using System.Runtime.CompilerServices;

namespace Game
{
	public static class ArrayUtils
	{
		public static T Random<T>(this Array array)
		{
			var index = UnityEngine.Random.Range(0, array.Length);
			return (T)array.GetValue(index);
		}
	}
}