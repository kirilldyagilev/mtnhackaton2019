﻿using UnityEngine;

namespace Game
{
	public class DamageMessage : Message
	{
		public int DamageValue;
		public IDamageController DamageController;
        public Transform attacker;
        public Transform target;
        public bool crit = false;
    }
}