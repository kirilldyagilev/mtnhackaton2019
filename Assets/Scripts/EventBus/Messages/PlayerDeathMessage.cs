﻿namespace Game
{
	public class PlayerDeathMessage : Message
	{
		public int Health;
		public int Attack;
		public int Stamina;
	}
}