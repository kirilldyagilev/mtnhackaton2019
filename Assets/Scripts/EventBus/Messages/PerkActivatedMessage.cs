﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class PerkActivatedMessage : Message
    {
        public bool enabled = true;
        public string perk;
    }
}
