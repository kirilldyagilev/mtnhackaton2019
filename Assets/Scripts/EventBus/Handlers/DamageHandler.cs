﻿using UnityEngine;

namespace Game
{
	public class DamageHandler : Handler<DamageMessage>
	{
        [SerializeField] private DamagePopup damagePopupPrefab;
		public override void HandleMessage(DamageMessage message)
		{
			message.DamageController.TryDamage(message.DamageValue);
            CreateDamagePopup(message.target, message.DamageValue, message.crit);

        }

        private void CreateDamagePopup(Transform target, int damage, bool crit)
        {
            var dmg = Instantiate(damagePopupPrefab);
            dmg.transform.position = target.position;
            dmg.Setup(damage, crit);
        }
	}
}