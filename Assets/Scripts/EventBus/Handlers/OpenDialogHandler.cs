﻿namespace Game
{
	public class OpenDialogHandler : Handler<OpenDialogMessage>
	{
		public DialogueSystem System;

		public override void HandleMessage(OpenDialogMessage message)
		{
			System.Open(message.Dialogue);
		}
	}
}