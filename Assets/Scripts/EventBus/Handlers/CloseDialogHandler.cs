﻿namespace Game
{
	public class CloseDialogHandler : Handler<CloseDialogMessage>
	{
		public DialogueSystem System;
		public override void HandleMessage(CloseDialogMessage message)
		{
			System.Close();
		}
	}
}