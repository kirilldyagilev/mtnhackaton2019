﻿namespace Game
{
	public class PlayerDamageHandler : Handler<DamagePlayer>
	{
		public override void HandleMessage(DamagePlayer message)
		{
			PlayerController.Instance.TryDamage(message.Damage);
		}
	}
}