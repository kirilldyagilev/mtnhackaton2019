﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game
{
    [RequireComponent(typeof(PerkController))]
    public class EnemyDeathHandler : Handler<EnemyDeathMessage>
    {
        private PerkController perkController;

        protected override void Awake()
        {
            base.Awake();
            perkController = GetComponent<PerkController>();
        }

        public override void HandleMessage(EnemyDeathMessage message)
        {
            if (perkController.IsEnabled(Perks.BloodThirst))
            {
                EventBus<PlayerHealMessage>.Pub(new PlayerHealMessage
                {
                    healValue = Convert.ToInt32(Math.Ceiling(message.deadMob.BaseData.Health * 0.2f))
                });
            }
        }
    }
}
