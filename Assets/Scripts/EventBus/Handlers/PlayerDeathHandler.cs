﻿using UnityEngine;

namespace Game
{
	public class PlayerDeathHandler : Handler<PlayerDeathMessage>
	{
		public GameObject _buttonObject = null;
		
		public override void HandleMessage(PlayerDeathMessage message)
		{
			_buttonObject.SetActive(true);
		}
	}
}