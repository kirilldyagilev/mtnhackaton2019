﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game
{
    public class PlayerHealHandler : Handler<PlayerHealMessage>
    {
        [SerializeField] private DamagePopup damagePopupPrefab;
        public override void HandleMessage(PlayerHealMessage message)
        {
            PlayerController.Instance.TryHeal(message.healValue);
            CreateDamagePopup(PlayerController.Instance.transform , -message.healValue, false);
        }

        private void CreateDamagePopup(Transform target, int damage, bool crit)
        {
            var dmg = Instantiate(damagePopupPrefab);
            dmg.transform.position = target.position;
            dmg.Setup(damage, crit);
        }
    }
}
