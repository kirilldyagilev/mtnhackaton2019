﻿using Game.Dungeon;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
namespace Game
{

	public static class ProfileOchestrator
	{
		static ProfileOchestrator()
		{
			Load();
			EventBus<EndEncounterMessage>.Sub(OnEncounterEnd);
		}
		public static ProfileData CurrentProfileData;
		private static string PathToFile => Path.Combine(Application.persistentDataPath, "Profile.json");

		public static int CurrentEncounter;
		public static int CurrentHP;
		public static List<string> Items = new List<string>();
		public static void Load()
		{
			if (!File.Exists(PathToFile))
			{
				CurrentProfileData = new ProfileData();
				Save();
			}
			using (var file = File.OpenText(PathToFile))
			{
				CurrentProfileData = ProfileData.Deserialize(file.ReadToEnd());
				CurrentHP = CurrentProfileData.HP;
			}
		}

#if UNITY_EDITOR
		[UnityEditor.MenuItem("Tools/RestProfile")]
#endif
		public static void Reset()
		{
			CurrentProfileData = new ProfileData();
			CurrentHP = CurrentProfileData.HP;
			Save();
		}

		public static void Save()
		{
			using (var writer = new StreamWriter(File.Create(PathToFile), System.Text.Encoding.UTF8))
			{
				writer.Write(CurrentProfileData.Serialize());
			}
		}
		public static void OnEncounterEnd(EndEncounterMessage message)
		{
			if (message.Perk != null)
			{
				if (!Items.Contains(message.Perk)) Items.Add(message.Perk);
			}
			if (message.EndType == EndEncounterMessage.Type.Complete)
			{
				CurrentProfileData.ActiveSeason++;
				CurrentEncounter = 0;
				CurrentHP = CurrentProfileData.HP;
				//Return to map
				EventBus<ReturnToMapMessage>.Pub(new ReturnToMapMessage());
				Items.Clear();
			}

			if (message.EndType == EndEncounterMessage.Type.SecondComics)
			{
				CurrentProfileData.ActiveSeason++;
				CurrentEncounter = 0;
				CurrentHP = CurrentProfileData.HP;
				//Return to map
				EventBus<LoadSceneMessage>.Pub(new LoadSceneMessage("SecondComics"));
				Items.Clear();
			}

			if (message.EndType == EndEncounterMessage.Type.FinalComics)
			{
				CurrentProfileData.ActiveSeason = 0;
				CurrentEncounter = 0;
				CurrentHP = CurrentProfileData.HP;
				//Return to map
				EventBus<LoadSceneMessage>.Pub(new LoadSceneMessage("CreatorsScene"));
				Items.Clear();
			}
			if (message.EndType == EndEncounterMessage.Type.Next)
			{
				CurrentEncounter++;
				//Launch next encounter
				EventBus<NextEncounterMessage>.Pub(new NextEncounterMessage());
			}
			if (message.EndType == EndEncounterMessage.Type.Death)
			{
				if (!CurrentProfileData.Deaths.ContainsKey(message.EncounterId))
				{
					CurrentProfileData.Deaths.Add(message.EncounterId, 0);
				}
				CurrentHP = CurrentProfileData.HP;
				CurrentProfileData.Deaths[message.EncounterId]++;
				CurrentEncounter = 0;
				//Return to map
				if (CurrentProfileData.ActiveSeason == 0)
				{
					EventBus<NextEncounterMessage>.Pub(new NextEncounterMessage());
				}
				else
				{
					EventBus<ReturnToMapMessage>.Pub(new ReturnToMapMessage());
				}
				Items.Clear();
			}

			CurrentProfileData.Attack += message.Attack;
			CurrentProfileData.HP += message.Health;
			CurrentProfileData.Stamina += message.Stamina;
			Save();
		}
	}

}