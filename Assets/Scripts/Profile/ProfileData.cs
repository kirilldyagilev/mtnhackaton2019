﻿using System.Collections.Generic;
namespace Game
{
	public class ProfileData
	{
		public int ActiveSeason = 0;
		public int HP = 175;
		public int Attack = 70;
		public int Stamina = 10;
		public List<string> Items;
		public Dictionary<int, int> Deaths;
		public int CurrentStoryIndex;
		public ProfileData()
		{
			Items = new List<string>();
			Deaths = new Dictionary<int, int>();
		}
		public string Serialize()
		{
			var json = JSONObject.Create();
			json.AddField("ActiveSeason", ActiveSeason);
			json.AddField("HP", HP);
			json.AddField("Attack", Attack);
			json.AddField("Stamina", Stamina);
			var items = JSONObject.Create(JSONObject.Type.ARRAY);
			foreach (var item in Items)
			{
				items.Add(item);
			}
			json.AddField("Items", items);
			var deaths = JSONObject.Create(JSONObject.Type.ARRAY);
			foreach (var death in Deaths)
			{
				deaths.Add(death.Key);
				deaths.Add(death.Value);
			}
			json.AddField("Deaths", deaths);
			json.AddField("CurrentStoryIndex", CurrentStoryIndex);
			return json.ToString();
		}

		public static ProfileData Deserialize(string json)
		{
			var data = new ProfileData();
			var jobject = JSONObject.Create(json);
			jobject.GetField(out data.ActiveSeason, "ActiveSeason", 0);
			jobject.GetField(out data.HP, "HP", 175);
			jobject.GetField(out data.Attack, "Attack", 70);
			jobject.GetField(out data.Stamina, "Stamina", 10);
			data.Items = new List<string>();
			var items = jobject["Items"];
			if (items != null)
			{
				foreach (var item in items)
				{
					data.Items.Add(item.str);
				}
			}
			data.Deaths = new Dictionary<int, int>();
			var deaths = jobject["Deaths"];
			if (deaths != null)
			{
				for (int i = 0; i < deaths.Count; i += 2)
				{
					data.Deaths.Add((int)deaths[i * 2].i, (int)deaths[i * 2 + 1].i);
				}
			}
			jobject.GetField(out data.CurrentStoryIndex, "CurrentStoryIndex", 0);
			return data;
		}
	}
}