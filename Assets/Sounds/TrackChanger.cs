﻿using UnityEngine;

namespace Game
{
	public class TrackChanger : MonoBehaviour
	{
		public AudioClip clip;
		private void Start()
		{
			SoundSystem.ChangeTrack(clip);
		}
	}
}
