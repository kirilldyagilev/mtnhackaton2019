﻿using UnityEngine;

namespace Game
{
    public class Sounds: MonoBehaviour
    {
        #region sounds
        public AudioClip ambient_autumn;
        public AudioClip ambient_spring;
        public AudioClip ambient_summer;
        public AudioClip ambient_winter;


        public AudioClip snd_enemy_footstep_ground1;
        public AudioClip snd_enemy_footstep_ground2;
        public AudioClip snd_enemy_footstep_ground3;
        public AudioClip snd_enemy_footstep_ground4;
        public AudioClip snd_enemy_footstep_snow1;
        public AudioClip snd_enemy_footstep_snow2;
        public AudioClip snd_enemy_footstep_snow3;
        public AudioClip snd_enemy_footstep_snow4;
        public AudioClip snd_enemy_mag_attack1;
        public AudioClip snd_enemy_mag_attack2;
        public AudioClip snd_enemy_mag_attack3;
        public AudioClip snd_enemy_mag_hit1;
        public AudioClip snd_enemy_mag_hit2;
        public AudioClip snd_enemy_mag_hit3;
        public AudioClip snd_enemy_shooter_attack1;
        public AudioClip snd_enemy_shooter_attack2;
        public AudioClip snd_enemy_shooter_attack3;
        public AudioClip snd_enemy_shooter_hit1;
        public AudioClip snd_enemy_shooter_hit2;
        public AudioClip snd_enemy_shooter_hit3;
        public AudioClip snd_enemy_tank_attack1;
        public AudioClip snd_enemy_tank_attack2;
        public AudioClip snd_enemy_tank_attack3;
        public AudioClip snd_enemy_tank_hit1;
        public AudioClip snd_enemy_tank_hit2;
        public AudioClip snd_enemy_tank_hit3;
        public AudioClip snd_enemy_tank_shield_impact1;
        public AudioClip snd_enemy_tank_shield_impact2;
        public AudioClip snd_enemy_tank_shield_impact3;
        public AudioClip snd_enemy_warrior_attack1;
        public AudioClip snd_enemy_warrior_attack2;
        public AudioClip snd_enemy_warrior_attack3;
        public AudioClip snd_enemy_warrior_hit1;
        public AudioClip snd_enemy_warrior_hit2;
        public AudioClip snd_enemy_warrior_hit3;
        public AudioClip snd_player_damage1;
        public AudioClip snd_player_damage2;
        public AudioClip snd_player_damage3;
        public AudioClip snd_player_damage4;
        public AudioClip snd_player_damage5;
        public AudioClip snd_player_death1;
        public AudioClip snd_player_death2;
        public AudioClip snd_player_death3;
        public AudioClip snd_player_death4;
        public AudioClip snd_player_death5;
        public AudioClip snd_player_fire1;
        public AudioClip snd_player_fire2;
        public AudioClip snd_player_fire3;
        public AudioClip snd_player_fire4;
        public AudioClip snd_player_footstep_ground1;
        public AudioClip snd_player_footstep_ground2;
        public AudioClip snd_player_footstep_ground3;
        public AudioClip snd_player_footstep_ground4;
        public AudioClip snd_player_footstep_snow1;
        public AudioClip snd_player_footstep_snow2;
        public AudioClip snd_player_footstep_snow3;
        public AudioClip snd_player_footstep_snow4;
        public AudioClip snd_player_hit1;
        public AudioClip snd_player_hit2;
        public AudioClip snd_player_hit3;
        public AudioClip snd_player_hit4;
        public AudioClip snd_player_move_roll1;
        public AudioClip snd_player_move_roll2;
        public AudioClip snd_player_move_roll3;


        public AudioClip snd_menu_car_ride;
        public AudioClip snd_menu_car_start;
        public AudioClip snd_menu_car_stay;
        public AudioClip snd_menu_car_stop;
        public AudioClip snd_menu_click;
        public AudioClip snd_menu_click2;
        public AudioClip snd_menu_idle_dialog_notification;
        public AudioClip snd_menu_ui_map;
        public AudioClip snd_menu_window_appear;
        public AudioClip snd_menu_window_disappear;

        public AudioClip music_gameplay_level_complete;
        public AudioClip music_gameplay_theme1;
        public AudioClip music_gameplay_theme2;
        public AudioClip music_gameplay_theme3;
        public AudioClip music_gameplay_theme4;
        public AudioClip music_loadingScreen_theme;
        public AudioClip music_main_menu_theme;

        #endregion

        public static Sounds instance;

        private void Awake()
        {
            if (instance != this)
                instance = this;
            DontDestroyOnLoad(this);
        }


    }
}
